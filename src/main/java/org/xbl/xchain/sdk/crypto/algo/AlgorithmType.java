package org.xbl.xchain.sdk.crypto.algo;

public enum AlgorithmType {

    ED25519("ed25519", "curve25519", "tendermint/PrivKeyEd25519", "tendermint/PubKeyEd25519"),
    SM2("sm2", "sm2p256v1", "tendermint/PrivKeySm2", "tendermint/PubKeySm2"),
    SECP256K1("secp256k1", "secp256k1", "tendermint/PrivKeySecp256k1", "tendermint/PubKeySecp256k1");

    public String name;
    public String curveName;

    public String priType;

    public String pubType;

    AlgorithmType(String name, String curveName, String priType, String pubType) {
        this.name = name;
        this.curveName = curveName;
        this.priType = priType;
        this.pubType = pubType;
    }

    public String getName() {
        return name;
    }

    public String getCurveName() {
        return curveName;
    }

    public String getPubType() {
        return pubType;
    }

    public String getPriType() {
        return priType;
    }

    public static AlgorithmType getAlgorithmTypeByName(String name) {
        for (AlgorithmType algorithmType : AlgorithmType.values()) {
            if (algorithmType.name.equals(name)) {
                return algorithmType;
            }
        }
        return null;
    }
}
