package org.xbl.xchain.sdk.crypto.algo;

public class AlgorithmFactory {
    public static Algorithm getAlgorithm(AlgorithmType cryptoType) {
        switch (cryptoType){
            case SM2:
                return SM2.getInstance();
            case SECP256K1:
                return Secp256k1.getInstance();
            case ED25519:
                return Ed25519.getInstance();
        }
        return null;
    }
}
