package org.xbl.xchain.sdk.tx;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.module.auth.types.Token;
import org.xbl.xchain.sdk.types.Fee;
import org.xbl.xchain.sdk.types.MsgTypeValue;
import org.xbl.xchain.sdk.types.TxConfig;
import org.xbl.xchain.sdk.utils.EncodeUtils;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@Data
public class StdSignBytesV2 {

    @JSONField(name = "chain_id")
    private String chainId;
    private Fee fee;
    private String memo;
    private MsgTypeValue[] msgs;
    private byte[] nonce;

    public StdSignBytesV2(byte[] nonce, String chainId, List<MsgTypeValue> msgs, TxConfig txConfig) {
        this.chainId = chainId;
        if (SysConfig.hasFee) {
            Fee fee = new Fee();
            fee.setAmount(new Token[]{});
            fee.setGas(txConfig.getGas());
            this.fee = fee;
        }
        this.memo = txConfig.getMemo();
        this.msgs = msgs.toArray(new MsgTypeValue[]{});
        this.nonce = nonce;
    }

    public StdSignBytesV2(byte[] nonce, String chainId, List<MsgTypeValue> msgs, Fee fee, String memo) {
        this.chainId = chainId;
        if (SysConfig.hasFee) {
            this.fee = fee;
        }
        this.memo = memo;
        this.msgs = msgs.toArray(new MsgTypeValue[]{});
        this.nonce = nonce;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("chain_id", chainId)
                .append("fee", fee)
                .append("memo", memo)
                .append("msgs", msgs)
                .append("nonce", nonce)
                .toString();
    }

    public String toJson() throws JsonProcessingException {
        //return Utils.serializer.toJson(this);
        return EncodeUtils.toJsonStringSortKeys(this);
    }
}
