package org.xbl.xchain.sdk.tx;

import lombok.Data;
import org.xbl.xchain.sdk.module.auth.types.Token;
import org.xbl.xchain.sdk.types.*;

import java.util.List;
import java.util.stream.Stream;

@Data
public class StdTx {
    private Msg[] msg;
    private Fee fee;
    private Signature[] signatures;
    private String memo;
    private byte[] nonce;

    public StdTx(List<? extends Msg> msgs, List<Signature> signatures, Long gas, String memo, byte[] nonce) {
        this.msg = msgs.toArray(new Msg[]{});
        this.signatures = signatures.toArray(new Signature[]{});
        Fee fee = new Fee();
        fee.setAmount(new Token[]{});
        fee.setGas(gas);
        this.fee = fee;
        this.memo = memo;
        this.nonce = nonce;
    }

    public StdTx() {
    }

    public String type() {
        return "xchain/StdTx";
    }

    public MsgTypeValue[] transferMsgTypeValues() {
        if (this.getMsg() == null || this.getMsg().length == 0) {
            return new MsgTypeValue[]{};
        }
        return Stream.of(this.getMsg()).map(Msg::transferMsgTypeValue).toArray(MsgTypeValue[]::new);
    }
}
