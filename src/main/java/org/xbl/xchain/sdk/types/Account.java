package org.xbl.xchain.sdk.types;

import lombok.Data;
import org.xbl.xchain.sdk.module.auth.querier.AuthAccount;

@Data
public class Account {
    KeyInfo keyInfo;

    AuthAccount authAccount;
    public static Account buildAccount(KeyInfo keyInfo) {
        Account account = new Account();
        account.setKeyInfo(keyInfo);
        return account;
    }

    public String getAddress() {
        return keyInfo.getAddress();
    }
}
