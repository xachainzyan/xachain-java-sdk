package org.xbl.xchain.sdk.types;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.xbl.xchain.sdk.amino.BytesInterface;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Signature {

    @JSONField(name = "pub_key")
    private BytesInterface pubkey;

    private byte[] signature;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("pub_key", pubkey)
            .append("signature", signature)
            .toString();
    }
}
