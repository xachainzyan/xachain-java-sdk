package org.xbl.xchain.sdk.types;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QueryResult<T> {
    private int code;
    private T result;
    private String error;
    public String toJsonString(){
        return JSON.toJSONString(this);
    }

    public QueryResult(int code, String error) {
        this.code = code;
        this.error = error;
    }
    public QueryResult() {
    }

    public QueryResult(int code) {
        this.code = code;
    }

    public static QueryResult success() {
        return new QueryResult(0);
    }
    public static QueryResult success(Object result) {
        QueryResult queryResult = new QueryResult(0);
        queryResult.setResult(result);
        return queryResult;
    }

    public static QueryResult fail(int code, String error) {
        return new QueryResult(code, error);
    }
}
