package org.xbl.xchain.sdk.types;

public enum TxMode {
    ASYNC("async", "broadcast_tx_async"), BLOCK("commit", "broadcast_tx_commit"), SYNC("sync", "broadcast_tx_sync");

    private String mode;

    private String rpcMethod;

    TxMode(String mode, String rpcMethod) {
        this.mode = mode;
        this.rpcMethod = rpcMethod;
    }

    public String getMode() {
        return mode;
    }

    public String getRpcMethod() {
        return rpcMethod;
    }
}

