package org.xbl.xchain.sdk.types;

import lombok.Data;
import org.xbl.xchain.sdk.tx.StdTx;

@Data
public class Envelop {

    private String mode;
    private StdTx tx;

    public Envelop() {
    }
}
