package org.xbl.xchain.sdk.types;

public class Address {
    public static String Bech32MainPrefix = "xchain";

    // Atom in https://github.com/satoshilabs/slips/blob/master/slip-0044.md
    public static int CoinType = 118;

    // BIP44Prefix is the parts of the BIP44 HD path that are fixed by
    // what we used during the fundraiser.
    public static String FullFundraiserPath = "44'/118'/0'/0/0";

    // PrefixAccount is the prefix for account keys
    public static String PrefixAccount = "acc";
    // PrefixValidator is the prefix for validator keys
    public static String PrefixValidator = "val";
    // PrefixConsensus is the prefix for consensus keys
    public static String PrefixConsensus = "cons";
    // PrefixPublic is the prefix for public keys
    public static String PrefixPublic = "pub";
    // PrefixOperator is the prefix for operator keys
    public static String PrefixOperator = "oper";

    // PrefixAddress is the prefix for addresses
    public static String PrefixAddress = "addr";

    // Bech32PrefixAccAddr defines the Bech32 prefix of an account's address
    public static String Bech32PrefixAccAddr = Bech32MainPrefix;
    // Bech32PrefixAccPub defines the Bech32 prefix of an account's public key
    public static String Bech32PrefixAccPub = Bech32MainPrefix + PrefixPublic;
    // Bech32PrefixValAddr defines the Bech32 prefix of a validator's operator address
    public static String Bech32PrefixValAddr = Bech32MainPrefix + PrefixValidator + PrefixOperator;
    // Bech32PrefixValPub defines the Bech32 prefix of a validator's operator public key
    public static String Bech32PrefixValPub = Bech32MainPrefix + PrefixValidator + PrefixOperator + PrefixPublic;
    // Bech32PrefixConsAddr defines the Bech32 prefix of a consensus node address
    public static String Bech32PrefixConsAddr = Bech32MainPrefix + PrefixValidator + PrefixConsensus;
    // Bech32PrefixConsPub defines the Bech32 prefix of a consensus node public key
    public static String Bech32PrefixConsPub = Bech32MainPrefix + PrefixValidator + PrefixConsensus + PrefixPublic;

    // GetBech32AccountAddrPrefix returns the Bech32 prefix for account address
    public static String GetBech32AccountAddrPrefix() {
        return Bech32PrefixAccAddr;
    }

    // GetBech32ValidatorAddrPrefix returns the Bech32 prefix for validator address
    public static String GetBech32ValidatorAddrPrefix()  {
        return Bech32PrefixValAddr;
    }

    // GetBech32ConsensusAddrPrefix returns the Bech32 prefix for consensus node address
    public static String GetBech32ConsensusAddrPrefix()  {
        return Bech32PrefixConsAddr;
    }

    // GetBech32AccountPubPrefix returns the Bech32 prefix for account public key
    public static String GetBech32AccountPubPrefix()  {
        return Bech32PrefixAccPub;
    }

    // GetBech32ValidatorPubPrefix returns the Bech32 prefix for validator public key
    public static String GetBech32ValidatorPubPrefix()  {
        return Bech32PrefixValPub;
    }

    // GetBech32ConsensusPubPrefix returns the Bech32 prefix for consensus node public key
    public static String GetBech32ConsensusPubPrefix()  {
        return Bech32PrefixConsPub;
    }

    public static String CharSet = "qpzry9x8gf2tvdw0s3jn54khce6mua7l";
}
