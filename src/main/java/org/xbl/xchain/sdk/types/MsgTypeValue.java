package org.xbl.xchain.sdk.types;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.xbl.xchain.sdk.amino.Codec;

@Data
public class MsgTypeValue {

    private String type;

    private Msg value;

    public MsgTypeValue(String type, Msg value) {
        this.type = type;
        this.value = value;
    }

    public MsgTypeValue() {
    }

    public static MsgTypeValue parseMsgTypeValueStr(String msgTypeValueStr){
        JSONObject jsonObject = JSON.parseObject(msgTypeValueStr);
        String type = jsonObject.getString("type");
        Class msgClassByType = Codec.getAmino().getMsgClassByType(type);
        Msg value = (Msg) jsonObject.getObject("value", msgClassByType);
        return new MsgTypeValue(type, value);
    }
}
