package org.xbl.xchain.sdk.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.xbl.xchain.sdk.module.auth.types.Token;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public class Fee {

    private Token[] amount;

    private Long gas;

    public Token[] getAmount() {
        return amount;
    }

    public Long getGas() {
        return gas;
    }

    public void setAmount(Token[] amount) {
        this.amount = amount;
    }

    public void setGas(Long gas) {
        this.gas = gas;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("amount", amount)
            .append("gas", gas)
            .toString();
    }
}
