package org.xbl.xchain.sdk.types;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
public abstract class Msg {
    public abstract String type();
    public abstract Exception ValidateBasic();
    public abstract String[] signers();

    public MsgTypeValue transferMsgTypeValue(){
        return new MsgTypeValue(this.type(), this);
    }
}
