package org.xbl.xchain.sdk.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.types.TxMode;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TxConfig {
    private String memo = "";
    private Long gas = 1000000000L;
    private TxMode txMode = TxMode.BLOCK;
}
