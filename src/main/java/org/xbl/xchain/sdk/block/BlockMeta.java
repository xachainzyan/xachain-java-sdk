package org.xbl.xchain.sdk.block;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class BlockMeta {

    @JSONField(name = "block_id")
    private BlockInfo.BlockId blockId;
    @JSONField(name = "block_size")
    private Long blockSize;
    private BlockInfo.BlockHeader header;
    @JSONField(name = "num_txs")
    private Long numTxs;
}
