package org.xbl.xchain.sdk.block;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.tx.StdTx;
import org.xbl.xchain.sdk.types.Fee;
import org.xbl.xchain.sdk.types.Msg;
import org.xbl.xchain.sdk.types.MsgTypeValue;
import org.xbl.xchain.sdk.types.Signature;

import java.util.List;
import java.util.stream.Stream;

@Data
public class StdTxInfo {
    private Msg[] msg;
    private Fee fee;
    private Signature[] signatures;
    private String memo;
    private byte[] nonce;
    private String txHash;

    public StdTxInfo(Msg[] msg, Fee fee, Signature[] signatures, String memo, byte[] nonce, String txHash) {
        this.msg = msg;
        this.fee = fee;
        this.signatures = signatures;
        this.memo = memo;
        this.nonce = nonce;
        this.txHash = txHash;
    }

    public StdTxInfo() {
    }

    public MsgTypeValue[] transferMsgTypeValues() {
        if (this.getMsg() == null || this.getMsg().length == 0) {
            return new MsgTypeValue[]{};
        }
        return Stream.of(this.getMsg()).map(Msg::transferMsgTypeValue).toArray(MsgTypeValue[]::new);
    }


}
