package org.xbl.xchain.sdk.block;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

@Data
public class BlockMetas {

    @JSONField(name = "last_height")
    private Long lastHeight;
    @JSONField(name = "block_metas")
    private List<BlockMeta> blockMetas;
}
