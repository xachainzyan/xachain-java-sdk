package org.xbl.xchain.sdk.gateway.init.BaseGatewayConfig;

public class Module {
    private String app;
    private String lite;
    private String executor;
    private String exchanger;
    private String monitor;
    private String syncer;
    private String appchain;

    public Module() {
        this.setApp("info");
        this.setLite("info");
        this.setExecutor("info");
        this.setExchanger("info");
        this.setMonitor("info");
        this.setSyncer("info");
        this.setAppchain("info");
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getLite() {
        return lite;
    }

    public void setLite(String lite) {
        this.lite = lite;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getExchanger() {
        return exchanger;
    }

    public void setExchanger(String exchanger) {
        this.exchanger = exchanger;
    }

    public String getMonitor() {
        return monitor;
    }

    public void setMonitor(String monitor) {
        this.monitor = monitor;
    }

    public String getSyncer() {
        return syncer;
    }

    public void setSyncer(String syncer) {
        this.syncer = syncer;
    }

    public String getAppchain() {
        return appchain;
    }

    public void setAppchain(String appchain) {
        this.appchain = appchain;
    }
}
