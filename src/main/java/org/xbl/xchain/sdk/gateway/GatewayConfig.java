package org.xbl.xchain.sdk.gateway;

import lombok.Data;

@Data
public class GatewayConfig {
    private String fabricConfigYaml;
    private String fabricConfigBYaml;
    private String fabricToml;
    private String gatewayToml;
    private String xchainConfigToml;

    public GatewayConfig() {
    }

    public GatewayConfig(String fabricConfigYaml, String fabricConfigBYaml, String fabricToml, String gatewatToml, String xchainConfigToml) {
        this.fabricConfigYaml = fabricConfigYaml;
        this.fabricConfigBYaml = fabricConfigBYaml;
        this.fabricToml = fabricToml;
        this.gatewayToml = gatewatToml;
        this.xchainConfigToml = xchainConfigToml;
    }

    public GatewayConfig(String gatewatToml, String xchainConfigToml) {
        this.gatewayToml = gatewatToml;
        this.xchainConfigToml = xchainConfigToml;
    }

    public GatewayConfig(String fabricConfigYaml, String fabricConfigBYaml, String fabricToml, String gatewatToml) {
        this.fabricConfigYaml = fabricConfigYaml;
        this.fabricConfigBYaml = fabricConfigBYaml;
        this.fabricToml = fabricToml;
        this.gatewayToml = gatewatToml;
    }
}
