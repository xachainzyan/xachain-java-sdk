package org.xbl.xchain.sdk.gateway.init.BaseGatewayConfig;


public class Log {
    private String level;
    private String dir;
    private String filename;
    private boolean reportCaller;

    public Log() {
        this.setLevel("info");
        this.setFilename("gateway.log");
        this.setDir("logs");
        this.setReportCaller(false);
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public boolean isReportCaller() {
        return reportCaller;
    }

    public void setReportCaller(boolean reportCaller) {
        this.reportCaller = reportCaller;
    }
}

