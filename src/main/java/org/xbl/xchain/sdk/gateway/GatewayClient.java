package org.xbl.xchain.sdk.gateway;

import org.xbl.xchain.sdk.gateway.init.GatewayToml;
import org.xbl.xchain.sdk.gateway.init.xchain.XchainConfigToml;

import java.lang.reflect.InvocationTargetException;

public class GatewayClient {
    public GatewayConfig init(String appChainId,String appChainNode,String appChainType,String hubChainId,String hubChainNode) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        GatewayToml gatewayToml = new GatewayToml(hubChainId,hubChainNode,appChainId,appChainType);
        XchainConfigToml cosmosToml = new XchainConfigToml(appChainId,appChainNode);
        return new GatewayConfig(gatewayToml.toTomlString(),cosmosToml.toTomlString());
    }
}
