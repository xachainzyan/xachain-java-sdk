package org.xbl.xchain.sdk.gateway.init.BaseGatewayConfig;

import org.xbl.xchain.sdk.utils.GenesisUtils;

import java.time.Duration;

public class HubChain {
    private String chainId;
    private String node;
    private String keyringBackend;
    private Duration trustPeriod;
    private int quorum;
    private int resendValsetTime;
    private int resendIccpTime;
    private Duration resendInterval;
    public HubChain(){
    }
    public HubChain(String chain_id, String node){
        if (chain_id.equals("")){
            chain_id="xchain";
        }
        if (node.equals("")){
            node="tcp://localhost:26657";
        }
        this.chainId = chain_id;
        this.node = node;
        this.keyringBackend="test";
        this.quorum=2;
        this.resendIccpTime=5;
        this.resendInterval=Duration.ofSeconds(5);
        this.resendValsetTime=5;
        this.trustPeriod=Duration.ofHours(200);
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getKeyringBackend() {
        return keyringBackend;
    }

    public void setKeyringBackend(String keyringBackend) {
        this.keyringBackend = keyringBackend;
    }

    public String getTrustPeriod() {
        long seca = this.trustPeriod.toNanos()/1000000000;
        return (seca/60/60)+"h"+(seca/60%60)+"m"+seca%60+"s";
    }

    public void setTrustPeriod(String trustPeriod) {

        int h=trustPeriod.indexOf("h");
        int m=trustPeriod.indexOf("m");
        int s=trustPeriod.indexOf("s");
        Duration hour=Duration.ofHours(0);
        Duration mun=Duration.ofMinutes(0);
        Duration sec=Duration.ofMillis(0);
        if (h>=0){
            hour=Duration.ofHours(Integer.parseInt(trustPeriod.substring(0,h)));
        }
        if (m>=0){
            mun=Duration.ofMinutes(Integer.parseInt(trustPeriod.substring(Math.max(h+1, 0),m)));
        }
        if  (s>=0){
            sec=Duration.ofMillis(1000L *Integer.parseInt(trustPeriod.substring(Math.max(m+1, 0),s)));
        }

        this.trustPeriod = hour.plus(mun).plus(sec);
    }

    public int getQuorum() {
        return quorum;
    }

    public void setQuorum(int quorum) {
        this.quorum = quorum;
    }

    public int getResendValsetTime() {
        return resendValsetTime;
    }

    public void setResendValsetTime(int resendValsetTime) {
        this.resendValsetTime = resendValsetTime;
    }

    public int getResendIccpTime() {
        return resendIccpTime;
    }

    public void setResendIccpTime(int resend_iccp_time) {
        this.resendIccpTime = resend_iccp_time;
    }

    public String getResendInterval() {
        return resendInterval.toMillis()/1000+"s";
    }

    public void setResendInterval(String resend_interval) {
        this.resendInterval = GenesisUtils.getDurationByString(resend_interval);
    }
}
