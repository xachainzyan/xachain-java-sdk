package org.xbl.xchain.sdk.gateway.init.fabric;

import com.moandjiezana.toml.TomlWriter;
import lombok.Data;
import org.xbl.xchain.sdk.utils.XcdUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

@Data
public class FabricToml {
    private String chainId;
    private String addr;
    private String eventFilter;
    private String username;
    private String ccid;
    private String channelId;
    private String org;

    public FabricToml(String chain_id, String addr, String event_filter, String username, String ccid, String channel_id, String org) {
        this.chainId = chain_id;
        this.addr = addr;
        this.eventFilter = event_filter;
        this.username = username;
        this.ccid = ccid;
        this.channelId = channel_id;
        this.org = org;
    }

    public String toTomlString() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TomlWriter tomlWriter = new TomlWriter();
        return tomlWriter.write(XcdUtil.ConvertObj2Map(new HashMap<String,Object>(),this,XcdUtil.UNDERLINE));
    }

}
