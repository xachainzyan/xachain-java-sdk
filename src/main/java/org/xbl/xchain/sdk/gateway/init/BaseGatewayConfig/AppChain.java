package org.xbl.xchain.sdk.gateway.init.BaseGatewayConfig;

import org.xbl.xchain.sdk.utils.GenesisUtils;

import java.time.Duration;

import static org.xbl.xchain.sdk.gateway.init.GatewayToml.Fabric;
import static org.xbl.xchain.sdk.gateway.init.GatewayToml.Xchain;

public class AppChain {
    private String chainId;
    private String plugin;
    private String config;
    private int resendValsetTime;
    private int resendIccpTime;
    private Duration resendInterval;

    public AppChain() {}

    public AppChain(String chain_id, String config) {
        if(chain_id==""){
            chain_id="xchain";
        }
        if (config!=Xchain&&config!=Fabric ){
            config = Xchain;
        }
        this.config=config;
        this.chainId = chain_id;
        this.plugin="/xchain.so";
        this.config=config;
        this.resendValsetTime=5;
        this.resendIccpTime=5;
        this.resendInterval= Duration.ofSeconds(5);
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public int getResendValsetTime() {
        return resendValsetTime;
    }

    public void setResendValsetTime(int resendValsetTime) {
        this.resendValsetTime = resendValsetTime;
    }

    public int getResendIccpTime() {
        return resendIccpTime;
    }

    public void setResendIccpTime(int resendIccpTime) {
        this.resendIccpTime = resendIccpTime;
    }

    public String getResendInterval() {
        return resendInterval.toMillis()/1000+"s";
    }

    public void setResendInterval(String resend_interval) {
        this.resendInterval = GenesisUtils.getDurationByString(resend_interval);
    }
}
