package org.xbl.xchain.sdk.utils;

/**
 * Http方法 枚举类
 * @Author zhw9026
 * @Date 2021-02-23
 **/
public enum HttpMethodType {

    POST, PUT, DELETE;
}
