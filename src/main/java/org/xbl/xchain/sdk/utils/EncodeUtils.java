package org.xbl.xchain.sdk.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class EncodeUtils {

    public static String toJsonStringSortKeys(Object object) {
        return JSON.toJSONString(object, new LongValueFilter(), SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.SortField);
    }

}
