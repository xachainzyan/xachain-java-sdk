package org.xbl.xchain.sdk.utils;

public class ErrorMsg {
    // RootCodespace is the codespace for all errors defined in this package
    public static String RootCodespace = "sdk";

    // UndefinedCodespace when we explicitly declare no codespace
    public static String UndefinedCodespace = "undefined";
    // errInternal should never be exposed, but we reserve this code for non-specified errors
    //nolint
    public static String errInternal = getErrorString(UndefinedCodespace, 1, "internal");

    // ErrTxDecode is returned if we cannot parse a transaction
    public static String ErrTxDecode = getErrorString(RootCodespace, 2, "tx parse error");

    // ErrInvalidSequence is used the sequence number (nonce) is incorrect
    // for the signature
    public static String ErrInvalidSequence = getErrorString(RootCodespace, 3, "invalid sequence");

    // ErrUnauthorized is used whenever a request without sufficient
    // authorization is handled.
    public static String ErrUnauthorized = getErrorString(RootCodespace, 4, "unauthorized");

    // ErrInsufficientFunds is used when the account cannot pay requested amount.
    public static String ErrInsufficientFunds = getErrorString(RootCodespace, 5, "insufficient funds");

    // ErrUnknownRequest to doc
    public static String ErrUnknownRequest = getErrorString(RootCodespace, 6, "unknown request");

    // ErrInvalidAddress to doc
    public static String ErrInvalidAddress = getErrorString(RootCodespace, 7, "invalid address");

    // ErrInvalidPubKey to doc
    public static String ErrInvalidPubKey = getErrorString(RootCodespace, 8, "invalid pubkey");

    // ErrUnknownAddress to doc
    public static String ErrUnknownAddress = getErrorString(RootCodespace, 9, "unknown address");

    // ErrInvalidCoins to doc
    public static String ErrInvalidCoins = getErrorString(RootCodespace, 10, "invalid coins");

    // ErrOutOfGas to doc
    public static String ErrOutOfGas = getErrorString(RootCodespace, 11, "out of gas");

    // ErrMemoTooLarge to doc
    public static String ErrMemoTooLarge = getErrorString(RootCodespace, 12, "memo too large");

    // ErrInsufficientFee to doc
    public static String ErrInsufficientFee = getErrorString(RootCodespace, 13, "insufficient fee");

    // ErrTooManySignatures to doc
    public static String ErrTooManySignatures = getErrorString(RootCodespace, 14, "maximum number of signatures exceeded");

    // ErrNoSignatures to doc
    public static String ErrNoSignatures = getErrorString(RootCodespace, 15, "no signatures supplied");

    // ErrJSONMarshal defines an ABCI typed JSON marshalling error
    public static String ErrJSONMarshal = getErrorString(RootCodespace, 16, "failed to marshal JSON bytes");

    // ErrJSONUnmarshal defines an ABCI typed JSON unmarshalling error
    public static String ErrJSONUnmarshal = getErrorString(RootCodespace, 17, "failed to unmarshal JSON bytes");

    // ErrInvalidRequest defines an ABCI typed error where the request contains
    // invalid data.
    public static String ErrInvalidRequest = getErrorString(RootCodespace, 18, "invalid request");

    // ErrTxInMempoolCache defines an ABCI typed error where a tx already exists
    // in the mempool.
    public static String ErrTxInMempoolCache = getErrorString(RootCodespace, 19, "tx already in mempool");

    // ErrMempoolIsFull defines an ABCI typed error where the mempool is full.
    public static String ErrMempoolIsFull = getErrorString(RootCodespace, 20, "mempool is full");

    // ErrTxTooLarge defines an ABCI typed error where tx is too large.
    public static String ErrTxTooLarge = getErrorString(RootCodespace, 21, "tx too large");

    public static int NoAlgorithmExceptionCode = 22;

    public static int SignExceptionCode = 23;

    public static String ErrHandle = getErrorString(RootCodespace, 99, "handle error");
    public static String ErrOrgId = getErrorString(RootCodespace, 100, "invalid orgId");
    public static String ErrOrgStatus = getErrorString(RootCodespace, 101, "invalid org status");
    public static String ErrRoleId = getErrorString(RootCodespace, 102, "invalid roleId");
    public static String ErrPolicy = getErrorString(RootCodespace, 103, "invalid policy");
    public static String ErrProposalId = getErrorString(RootCodespace, 104, "invalid proposalId");
    public static String ErrEditType = getErrorString(RootCodespace, 105, "invalid editType");
    public static String ErrOwner = getErrorString(RootCodespace, 106, "invalid owner");
    public static String ErrAddress = getErrorString(RootCodespace, 107, "invalid address");
    public static String ErrParentOrgId = getErrorString(RootCodespace, 108, "invalid orgId");
    public static String ErrBaseRoleId = getErrorString(RootCodespace, 109, "invalid baseRoleId");
    public static String ErrProposalAlreadyExist = getErrorString(RootCodespace, 110, "proposal already exists");
    public static String ErrNotFoundProposal = getErrorString(RootCodespace, 111, "not fount proposal");
    public static String ErrPermissionAlreadyExist = getErrorString(RootCodespace, 112, "permission already exists");

    public static String ErrResouce = getErrorString(RootCodespace, 23, "invalid Resouce");
    public static String ErrAccount = getErrorString(RootCodespace, 24, "invalid account");
    public static String ErrCheckPermission = getErrorString(RootCodespace, 25, "Check Permission error");

    public static String ErrExistName = getErrorString(RootCodespace, 26, "exist name");

    // ErrPanic is only set when we recover from a panic, so we know to
    // redact potentially sensitive system info
    public static String ErrPanic = getErrorString(UndefinedCodespace, 111222, "panic");
    public static String getErrorString(String codespace, int code, String info){
        return String.format("[%s] %d %s",codespace,code,info);
    }
}
