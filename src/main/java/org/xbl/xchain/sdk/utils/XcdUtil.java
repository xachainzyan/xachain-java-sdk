package org.xbl.xchain.sdk.utils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class XcdUtil {
    public static final char UNDERLINE = '_';
    public static final char MidLINE = '-';
    public static HashMap<String, Object> ConvertObj2Map(HashMap<String, Object> map, Object obj, char flag) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Field[] fields = obj.getClass().getDeclaredFields();
        for(Field field:fields){
            field.setAccessible(true);
            String fieldName = field.getName();
            char[] chars = fieldName.toCharArray();
            chars[0] = Character.toUpperCase(chars[0]);
            String getMethodName="";
            Method fieldGetMet;
            if (field.getType()==boolean.class){
                getMethodName = "is" + new String(chars);
            }else{
                getMethodName = "get" + new String(chars);
            }
            fieldGetMet = obj.getClass().getMethod(getMethodName);
            map.put(CamelToUnderline(field.getName(),1,flag), fieldGetMet.invoke(obj));
        }
        return map;
    }

    //驼峰转下划线
    public static String CamelToUnderline(String param, Integer charType, char flag) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        if (param.equals("P2P")){
            return "p2p";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c)) {
                if (i>0) {
                    sb.append(flag);
                }
            }
            if (charType == 2) {
                sb.append(Character.toUpperCase(c));  //统一都转大写
            } else {
                sb.append(Character.toLowerCase(c));  //统一都转小写
            }


        }
        return sb.toString();
    }
    //下划线转驼峰
    public static String UnderlineToCamel(String param, Integer charType) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c==UNDERLINE){
                if (i<len-1){
                    sb.append(Character.toUpperCase(param.charAt(++i)));
                }
            }else{
                if (charType == 2) {
                    sb.append(Character.toUpperCase(c));  //统一都转大写
                } else {
                    sb.append(Character.toLowerCase(c));  //统一都转小写
                }
            }
        }
        return sb.toString();
    }
    public static void ExecMethodByName(Object object, String name, Object value) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Class c = object.getClass();
        PropertyDescriptor pd = new PropertyDescriptor(name, c);
        //获得Set方法
        Method method = pd.getWriteMethod();
        if (pd.getPropertyType() == int.class || pd.getPropertyType() == Integer.class){
            if (value.getClass()== Long.class){
                method.invoke(object, ((Long) value).intValue());
            }
        } else{
            method.invoke(object, value);
        }
    }
}
