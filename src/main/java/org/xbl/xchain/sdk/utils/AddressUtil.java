package org.xbl.xchain.sdk.utils;

import org.xbl.xchain.sdk.crypto.encode.Bech32;
import org.xbl.xchain.sdk.crypto.encode.ConvertBits;

public class AddressUtil {

    public static byte[] accAddressFromBech32(String address){
        byte[] dec = Bech32.decode(address).getData();
        return ConvertBits.convertBits(dec, 0, dec.length, 5, 8, false);
    }
    public static String accAddressToBech32(String mainPrefix, byte[] bz){
        byte[] bytes = ConvertBits.convertBits(bz, 0, bz.length, 8, 5, true);
        return Bech32.encode(mainPrefix, bytes);
    }
}
