package org.xbl.xchain.sdk;

import lombok.Data;
import org.xbl.xchain.sdk.crypto.algo.*;
import org.xbl.xchain.sdk.types.Version;

@Data
public class SysConfig {

    public static final String HD_PATH = "M/44H/118H/0H/0/0";

    public static final String VALIDATOR_ADDR_PREFIX = "val";

    public static final String REST_PATH_PREFIX = "";


    public static final boolean hasFee = true;

    // 1: sequence 2 nonce
    private String version = Version.NONCE;
    private String rpcUrl = "http://127.0.0.1:26657";

    private String chainId = "xachain";

    private String mainPrefix = "xchain";

    private AlgorithmType defaultCryptoType = AlgorithmType.SM2;

    private String denom = "stake";

    private String serverCertPath;

    public SysConfig(String rpcUrl, String chainId, String mainPrefix, AlgorithmType cryptoType) {
        this.rpcUrl = rpcUrl;
        this.chainId = chainId;
        this.mainPrefix = mainPrefix;
        this.defaultCryptoType = cryptoType;
    }

    public SysConfig(String rpcUrl, String chainId, AlgorithmType cryptoType) {
        this.rpcUrl = rpcUrl;
        this.chainId = chainId;
        this.defaultCryptoType = cryptoType;
    }

    public SysConfig(String rpcUrl, String chainId, String version) {
        this.rpcUrl = rpcUrl;
        this.chainId = chainId;
        this.version = version;
    }

    public SysConfig(String rpcUrl, String chainId) {
        this.rpcUrl = rpcUrl;
        this.chainId = chainId;
    }



    public SysConfig(String rpcUrl) {
        this.rpcUrl = rpcUrl;
    }

    public SysConfig() {
    }

    public boolean isHttps() {
        return getRpcUrl().startsWith("https") ? true : false;
    }

    public Object getPassword() {
        return null
                ;
    }
}
