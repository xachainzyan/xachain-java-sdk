package org.xbl.xchain.sdk.amino;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class FieldNumberAndTyp3 {

    private Boolean success;

    private Integer fieldNum;

    private Integer typ3;

    private Integer offset;

    public FieldNumberAndTyp3(Boolean success) {
        this.success = success;
    }

    public static FieldNumberAndTyp3 success(Integer fieldNum, Integer typ3, Integer offset) {
        return new FieldNumberAndTyp3(true, fieldNum, typ3, offset);

    }

    public static FieldNumberAndTyp3 faild() {
        return new FieldNumberAndTyp3(false);
    }
}
