package org.xbl.xchain.sdk.amino;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor
@AllArgsConstructor
public class BytesInterface {

    private byte[] value;

    private String type;

    public String type(){
        return StringUtils.isNotBlank(type) ? type : "";
    }

    public byte[] getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
