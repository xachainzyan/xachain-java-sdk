package org.xbl.xchain.sdk.amino;

import org.apache.commons.lang3.StringUtils;

public enum SerializeType {
    ADDRESS("address", byte[].class),
    PUBKEY("pubkey", BytesInterface.class),
    GOTIME("time", GoTime.class);
    private String type;

    private Class deSerializeclazz;


    SerializeType(String type, Class deSerializeclazz) {
        this.type = type;
        this.deSerializeclazz = deSerializeclazz;
    }

    public String getType() {
        return type;
    }

    public Class getDeSerializeclazz() {
        return deSerializeclazz;
    }

    public static SerializeType valueOfByType(String type){
        if (StringUtils.isNotBlank(type)){
            for (SerializeType serializeType : SerializeType.values()) {
                if (serializeType.getType().equals(type)) {
                    return serializeType;
                }
            }
        }
        return null;
    }
}
