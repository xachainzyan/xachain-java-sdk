package org.xbl.xchain.sdk.amino;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AminoFieldSerialize {

    String format() default "address";
}
