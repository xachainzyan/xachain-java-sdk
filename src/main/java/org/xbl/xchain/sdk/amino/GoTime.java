package org.xbl.xchain.sdk.amino;

import lombok.Data;
import org.xbl.xchain.sdk.utils.GenesisUtils;

import java.text.ParseException;
import java.util.Date;

@Data
public class GoTime {
    private long wall;
    private long ext;

    public String toTimeStr(){
        Date date = new Date(this.getWall()*1000L+this.getExt()/1000000L);
        return GenesisUtils.UTCDateFormat.format(date);
    }

    public GoTime() {
    }

    public GoTime(String timestr) {
        if(null==timestr||"".equals(timestr))return;
        Date date;
        try {
            date = GenesisUtils.UTCDateFormat.parse(timestr);
        } catch (ParseException e) {
            e.printStackTrace();
            date = new Date();
        }
        long datelong = date.getTime();
        this.wall=datelong/1000L;
        this.ext =datelong%1000L*1000000L;
    }
}
