package org.xbl.xchain.sdk.amino;

import org.xbl.xchain.sdk.types.Msg;

import java.util.List;

public class TypeUtils {
    public static Boolean isByteArray(Class clazz) {
        return clazz == byte[].class;
    }

    public static Boolean isArrayOrList(Class clazz) {
        return isList(clazz) || isArray(clazz);
    }

    public static Boolean isArray(Class clazz) {
        return clazz.isArray() && !isByteArray(clazz);
    }

    public static Boolean isList(Class clazz) {
        return List.class.isAssignableFrom(clazz);
    }

    public static Boolean isString(Class clazz) {
        return String.class.isAssignableFrom(clazz);
    }

    public static Boolean isInterface(Class clazz) {
        return Msg.class.isAssignableFrom(clazz) || isBytesInterface(clazz);
    }

    public static Boolean isBytesInterface(Class clazz) {
        return BytesInterface.class.isAssignableFrom(clazz);
    }

    public static Boolean isNumber(Class clazz) {
        return Number.class.isAssignableFrom(clazz);
    }

    public static Boolean isLong(Class clazz) {
        return Long.class.isAssignableFrom(clazz) || long.class.isAssignableFrom(clazz);
    }

    public static Boolean isInteger(Class clazz) {
        return Integer.class.isAssignableFrom(clazz) || int.class.isAssignableFrom(clazz);
    }

    public static Boolean isBoolean(Class clazz) {
        return Boolean.class.isAssignableFrom(clazz) || boolean.class.isAssignableFrom(clazz);
    }

    public static Boolean isStruct(Class clazz) {
        return Object.class.isAssignableFrom(clazz);
    }
}
