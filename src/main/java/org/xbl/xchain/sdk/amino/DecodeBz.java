package org.xbl.xchain.sdk.amino;

import lombok.Data;

@Data
public class DecodeBz {

    private byte[] bz;

    private byte[] tempPrefix;

    public void cutBytes(int start) {
        int newLength = bz.length - start;
        byte[] newD = new byte[newLength];
        System.arraycopy(bz, start, newD, 0, newLength);
        bz = newD;
    }

    public byte[] popBytes(int start, int length) {
        byte[] data = new byte[length];
        System.arraycopy(bz, start, data, 0, length);
        return data;
    }

    public DecodeBz(byte[] bz) {
        this.bz = bz;
    }

    public byte[] getBz() {
        return bz;
    }
}
