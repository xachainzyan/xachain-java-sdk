package org.xbl.xchain.sdk.amino;

import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.module.auth.msg.MsgTransfer;
import org.xbl.xchain.sdk.module.celerain.msg.MsgCelerain;
import org.xbl.xchain.sdk.module.consensus.msg.MsgCreateValidator;
import org.xbl.xchain.sdk.module.consensus.msg.MsgEditValidator;
import org.xbl.xchain.sdk.module.cross.msg.MsgCrossFromOrigin;
import org.xbl.xchain.sdk.module.cross.msg.MsgRelayValidators;
import org.xbl.xchain.sdk.module.gov.msg.MsgVoidProposal;
import org.xbl.xchain.sdk.module.gov.msg.MsgVote;
import org.xbl.xchain.sdk.module.iccp.msg.MsgAppChain;
import org.xbl.xchain.sdk.module.iccp.msg.MsgCross;
import org.xbl.xchain.sdk.module.iccp.msg.MsgRelayCross;
import org.xbl.xchain.sdk.module.iccp.msg.MsgUpdateValidator;
import org.xbl.xchain.sdk.module.member.msg.*;
import org.xbl.xchain.sdk.module.wasm.msg.*;
import org.xbl.xchain.sdk.tx.StdTx;
import org.xbl.xchain.sdk.types.Version;

public class Codec {
    private static Amino amino = null;
    private static Amino aminoV1 = null;

    static {
        amino = new Amino();
        amino.registerConcrete(BytesInterface.class, AlgorithmType.SM2.getPubType());
//        amino.registerConcrete(AlgorithmType.SM2.getPriType());
        amino.registerConcrete(BytesInterface.class, AlgorithmType.SECP256K1.getPubType());
//        amino.registerConcrete(AlgorithmType.SECP256K1.getPriType());
        amino.registerConcrete(BytesInterface.class, AlgorithmType.ED25519.getPubType());
//        amino.registerConcrete(AlgorithmType.ED25519.getPriType());
        amino.registerConcrete(StdTx.class, "xchain/StdTx");
        amino.registerConcrete(MsgAddAccount.class, "xchain/AddAccount");
        amino.registerConcrete(MsgRevokeAccount.class, "xchain/RevokeAccount");
        amino.registerConcrete(MsgEditAccountRole.class, "xchain/EditAccountRole");
        amino.registerConcrete(MsgFreezeAccount.class, "xchain/FreezeAccount");
        amino.registerConcrete(MsgUnFreezeAccount.class, "xchain/UnfreezeAccount");
        amino.registerConcrete(MsgAddGateway.class,"xchain/AddGateway");
        amino.registerConcrete(MsgRevokeGateway.class, "xchain/RevokeGateway");
        amino.registerConcrete(MsgAddNetworkAdmin.class,"xchain/AddNwAdmin");
        amino.registerConcrete(MsgRevokeNetworkAdmin.class, "xchain/RevokeNwAdmin");
        amino.registerConcrete(MsgAddOrg.class, "xchain/AddOrg");
        amino.registerConcrete(MsgRevokeOrg.class,"xchain/RemoveOrg");
        amino.registerConcrete(MsgAddSubOrg.class,"xchain/AddSubOrg");
        amino.registerConcrete(MsgRevokeSubOrg.class,"xchain/RemoveSubOrg");
        amino.registerConcrete(MsgFreezeOrg.class, "xchain/FreezeOrg");
        amino.registerConcrete(MsgUnFreezeOrg.class,"xchain/UnfreezeOrg");
        amino.registerConcrete(MsgChangeOrgAdmin.class,"xchain/ChangeOrgAdmin");
        amino.registerConcrete(MsgAddRole.class,"xchain/AddRole");
        amino.registerConcrete(MsgRevokeRole.class,"xchain/RemoveRole");
        amino.registerConcrete(MsgSavePermission.class,"xchain/SavePermission");
        amino.registerConcrete(MsgCreateValidator.class,"xchain/MsgCreateValidator");
        amino.registerConcrete(MsgEditValidator.class,"xchain/MsgEditValidator");
        amino.registerConcrete(MsgVote.class,"xchain/vote");
        amino.registerConcrete(MsgVoidProposal.class,"xchain/voidProposal");
        amino.registerConcrete(MsgTransfer.class,"xchain/MsgSend");
        //合约
        amino.registerConcrete(MsgInstantiateContract.class,"wasm/MsgInstantiateContract");
        amino.registerConcrete(MsgMigrateContract.class,"wasm/MsgMigrateContract");
        amino.registerConcrete(MsgExecuteContract.class,"wasm/MsgExecuteContract");
        amino.registerConcrete(MsgDestroyContract.class,"wasm/MsgDestroyContract");
        amino.registerConcrete(MsgUpdatePermission.class,"wasm/MsgUpdatePermission");
        amino.registerConcrete(MsgCelerain.class,"celerain/MsgCelerain");
        //跨链
        amino.registerConcrete(MsgAppChain.class,"xchain/MsgAppChain");
        amino.registerConcrete(MsgCross.class,"xchain/MsgCross");
        amino.registerConcrete(MsgCrossFromOrigin.class,"xchain/MsgCrossFromOrigin");
        amino.registerConcrete(MsgRelayCross.class,"xchain/MsgRelayCross");
        amino.registerConcrete(MsgRelayValidators.class,"xchain/MsgRelayValidators");
        amino.registerConcrete(MsgUpdateValidator.class,"xchain/MsgUpdateValidator");

        aminoV1 = new Amino();
        aminoV1.registerConcrete(BytesInterface.class, AlgorithmType.SM2.getPubType());
//        amino.registerConcrete(AlgorithmType.SM2.getPriType());
        aminoV1.registerConcrete(BytesInterface.class, AlgorithmType.SECP256K1.getPubType());
//        amino.registerConcrete(AlgorithmType.SECP256K1.getPriType());
        aminoV1.registerConcrete(BytesInterface.class, AlgorithmType.ED25519.getPubType());
//        amino.registerConcrete(AlgorithmType.ED25519.getPriType());
        aminoV1.registerConcrete(StdTx.class, "xchain/StdTx");
        aminoV1.registerConcrete(MsgAddAccountV1.class, "xchain/AddAccount");
        aminoV1.registerConcrete(MsgRevokeAccount.class, "xchain/RevokeAccount");
        aminoV1.registerConcrete(MsgEditAccountRole.class, "xchain/EditAccountRole");
        aminoV1.registerConcrete(MsgFreezeAccount.class, "xchain/FreezeAccount");
        aminoV1.registerConcrete(MsgUnFreezeAccount.class, "xchain/UnfreezeAccount");
        aminoV1.registerConcrete(MsgAddGateway.class,"xchain/AddGateway");
        aminoV1.registerConcrete(MsgRevokeGateway.class, "xchain/RevokeGateway");
        aminoV1.registerConcrete(MsgAddNetworkAdmin.class,"xchain/AddNwAdmin");
        aminoV1.registerConcrete(MsgRevokeNetworkAdmin.class, "xchain/RevokeNwAdmin");
        aminoV1.registerConcrete(MsgAddOrg.class, "xchain/AddOrg");
        aminoV1.registerConcrete(MsgRevokeOrg.class,"xchain/RemoveOrg");
        aminoV1.registerConcrete(MsgAddSubOrg.class,"xchain/AddSubOrg");
        aminoV1.registerConcrete(MsgRevokeSubOrg.class,"xchain/RemoveSubOrg");
        aminoV1.registerConcrete(MsgFreezeOrg.class, "xchain/FreezeOrg");
        aminoV1.registerConcrete(MsgUnFreezeOrg.class,"xchain/UnfreezeOrg");
        aminoV1.registerConcrete(MsgChangeOrgAdmin.class,"xchain/ChangeOrgAdmin");
        aminoV1.registerConcrete(MsgAddRole.class,"xchain/AddRole");
        aminoV1.registerConcrete(MsgRevokeRole.class,"xchain/RemoveRole");
        aminoV1.registerConcrete(MsgSavePermission.class,"xchain/SavePermission");
        aminoV1.registerConcrete(MsgCreateValidator.class,"xchain/MsgCreateValidator");
        aminoV1.registerConcrete(MsgEditValidator.class,"xchain/MsgEditValidator");
        aminoV1.registerConcrete(MsgVote.class,"xchain/vote");
        aminoV1.registerConcrete(MsgVoidProposal.class,"xchain/voidProposal");
        aminoV1.registerConcrete(MsgTransfer.class,"xchain/MsgSend");
        //合约
        aminoV1.registerConcrete(MsgInstantiateContractV1.class,"wasm/MsgInstantiateContract");
        aminoV1.registerConcrete(MsgMigrateContractV1.class,"wasm/MsgMigrateContract");
        aminoV1.registerConcrete(MsgExecuteContractV1.class,"wasm/MsgExecuteContract");
        aminoV1.registerConcrete(MsgDestroyContract.class,"wasm/MsgDestroyContract");
        aminoV1.registerConcrete(MsgUpdatePermission.class,"wasm/MsgUpdatePermission");
        aminoV1.registerConcrete(MsgCelerain.class,"celerain/MsgCelerain");
        //跨链
        aminoV1.registerConcrete(MsgAppChain.class,"xchain/MsgAppChain");
        aminoV1.registerConcrete(MsgCross.class,"xchain/MsgCross");
        aminoV1.registerConcrete(MsgCrossFromOrigin.class,"xchain/MsgCrossFromOrigin");
        aminoV1.registerConcrete(MsgRelayCross.class,"xchain/MsgRelayCross");
        aminoV1.registerConcrete(MsgRelayValidators.class,"xchain/MsgRelayValidators");
        aminoV1.registerConcrete(MsgUpdateValidator.class,"xchain/MsgUpdateValidator");
    }

    public static Amino getAmino() {
        return amino;
    }
    public static Amino getAmino(String version) {
        if (Version.SEQUENCE.equals(version)) {
            return aminoV1;
        }
        return amino;
    }
}
