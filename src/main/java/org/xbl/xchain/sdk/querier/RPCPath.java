package org.xbl.xchain.sdk.querier;

public interface RPCPath {
    String HEALTH = "health";
    String DUMP_CONSENSUS_STATE = "dump_consensus_state";

    String STATUS = "status";
    String ABCI_QUERY = "abci_query";
    String BLOCK = "block";
    String BLOCK_BY_HASH = "block_by_hash";
    String BLOCKCHAIN = "blockchain";
    String TX = "tx";
    String TX_SEARCH = "tx_search";


}
