package org.xbl.xchain.sdk.querier;

public interface ABCIPath {

    /*member*/
    String AUTH_ACCOUNTS = "custom/acc/account";
    String MEMBER_ORGS = "custom/member/orgs";
    String MEMBER_ORG = "custom/member/org/%s";
    String MEMBER_NWADMINS = "/member/nwadmins";
    String MEMBER_ROLES = "custom/member/roles";
    String MEMBER_ROLE = "custom/member/role/%s";
    String MEMBER_ACCOUNTS = "custom/member/accounts";
    String MEMBER_ACCOUNT = "custom/member/account/%s";
    String MEMBER_PERMISSIONS = "custom/member/permissions";
    String MEMBER_PERMISSION = "custom/member/permission/%s";
    String MEMBER_CHECKPERMISSION = "custom/member/checkPermission/%s/%s";

    /*wasm*/
    String WASM_CONTRACT = "custom/wasm/contract-state/%s/smart";
    String WASM_CONTRACT_INFOS = "custom/wasm/list-contracts";
    String WASM_CONTRACT_INFO = "custom/wasm/contract-info/%s";
    String WASM_CONTRACT_HISTORY = "custom/wasm/contract-history/%s";
    /*gov*/
    String GOV_PROPOSALS = "custom/gov/proposals";
    String GOV_PROPOSAL = "custom/gov/proposal/%s";
    /*poa*/
    String POA_VALIDATORS = "custom/poa/validators";
    String POA_VALIDATOR = "custom/poa/validator";
    /**/
    String CELERAIN_CELERAIN = "custom/celerain/celerain/%s";
    /**/
    String ICCP_APPCHAININFO = "custom/iccp/iccpinfo/%s";
    String ICCP_APPCHAININFO_STORE = "store/iccp/key";
    /**/

}
