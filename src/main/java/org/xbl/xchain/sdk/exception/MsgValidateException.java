package org.xbl.xchain.sdk.exception;

public class MsgValidateException extends Exception{
    public MsgValidateException() {
        super();
    }

    public MsgValidateException(String message) {
        super(message);
    }
}
