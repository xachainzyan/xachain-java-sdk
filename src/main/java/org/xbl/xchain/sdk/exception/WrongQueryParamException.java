package org.xbl.xchain.sdk.exception;

public class WrongQueryParamException extends Exception {

    public WrongQueryParamException(String message) {
        super(message);
    }
}
