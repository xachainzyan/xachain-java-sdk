package org.xbl.xchain.sdk.module.member.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 *  新增账户RestAPI反馈信息 实体类
 *  @Author zhw9026
 *  @Date 2021-02-20
 */
@Data
@AllArgsConstructor
public class MsgAddRole extends Msg {

    private String roleId;
    private String baseRoleId;
    private String orgFullId;
    @AminoFieldSerialize(format = "address")
    private String owner;

    public MsgAddRole() {
    }
    @Override
    public String type() {
        return "xchain/AddRole";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }

}
