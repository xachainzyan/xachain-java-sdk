package org.xbl.xchain.sdk.module.iccp.types;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppChainInfo {
    // 应用链的id，不同应用链的id必须不同
    @JSONField(name = "chain_id")
    private String chainId;
    // 应用链的昵称，这个可以重复
    private String name;
    // 应用链的类型，cosmos or fabric
    @JSONField(name = "chain_type")
    private String chainType;
    // 共识类型
    @JSONField(name = "consensus_type")
    private int consensusType;
    // 应用链验证者公钥集合
    private String validators;
    // 应用链的版本号
    private String version;
    // 应用链描述
    private String desc;
    // 应用链管理者的公钥
    @JSONField(name = "public_key")
    private String publicKey;
    // 应用链在核心链绑定的状态
    private int state;
    // 注册成功的提案 id
    @JSONField(name = "proposal_id")
    private String proposalId;
}
