package org.xbl.xchain.sdk.module.wasm.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgInstantiateContract extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    @JSONField(name = "wasm_byte_code")
    private byte[] wasmBytes;       //合约二进制

    @JSONField(name = "execute_perm")
    private String executePerm;     // format  (Org1&member||Org2,Org3&member,admin)

    private String language;        // only "golang" or "rust"

    private Integer policy;          // 1--白名单 2--黑名单

    @AminoFieldSerialize(format = "address")
    private String admin;

    private String label;

    @JSONField(name = "contract_name")
    private String contractName;

    @JSONField(name = "init_msg")
    private String initMsg;

    public MsgInstantiateContract() {
    }

    @Override
    public String type() {
        return "wasm/MsgInstantiateContract";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
