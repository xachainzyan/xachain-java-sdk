package org.xbl.xchain.sdk.module.member.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 撤销账号RestAPI反馈信息 实体类
 * @Author zhw9026
 * @Date 2021-02-23
 **/
@Data
@AllArgsConstructor
public class MsgRevokeAccount extends Msg {

    //撤销的账号地址
    private String address;

    @AminoFieldSerialize(format = "address")
    private String owner;

    public MsgRevokeAccount() {
    }

    @Override
    public String type() {
        return "xchain/RevokeAccount";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }

}
