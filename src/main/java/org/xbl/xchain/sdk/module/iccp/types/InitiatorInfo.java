package org.xbl.xchain.sdk.module.iccp.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class InitiatorInfo {

    @JSONField(name = "chain_id")
    private String chainId;

    @JSONField(name = "chain_name")
    private String chainName;

    @JSONField(name = "chain_type")
    private String chainType;

    @JSONField(name = "chain_version")
    private String chainVersion;

    @JSONField(name = "org_id")
    private String orgId;

    private String[] roles;

    private String address;
}
