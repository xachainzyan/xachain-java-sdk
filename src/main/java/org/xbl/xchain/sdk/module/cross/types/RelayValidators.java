package org.xbl.xchain.sdk.module.cross.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class RelayValidators {
    @JSONField(name = "chain_id")
    private String chainId ;
    private Long height ;
    @JSONField(name = "validator_set")
    private ValidatorSet validatorSet ;
    @JSONField(name = "next_validator_set")
    private ValidatorSet nextValidatorSet ;
}
