package org.xbl.xchain.sdk.module.wasm.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.auth.types.Token;
import org.xbl.xchain.sdk.types.Msg;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
public class MsgExecuteContractV1 extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    @AminoFieldSerialize(format = "address")
    private String origin;

    private String contract;
    @JSONField(name = "contract_name")
    private String contractName;

    @JSONField(name = "msg")
    private byte[] args;

    @JSONField(name = "sent_funds")
    private Token[] sentFunds;

    public MsgExecuteContractV1() {
    }

    public MsgExecuteContractV1(String contractName, String sender, String args) {
        super();
        this.contractName = contractName;
        this.sender = sender;
        this.origin = sender;
        this.args = args.getBytes();
        this.contract = "";
        this.sentFunds = new Token[]{};
    }

    @Override
    public String type() {
        return "wasm/MsgExecuteContract";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}