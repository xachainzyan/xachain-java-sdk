package org.xbl.xchain.sdk.module.auth.querier;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.module.auth.types.Token;

import java.util.List;

@Data
public class AuthAccount {

    private String type;

    private Account value;

    @Data
    public class Account {
        private String address;
        private List<Token> coins;
        @JSONField(name = "account_number")
        private Long accNum;
        private Long sequence;
    }
}
