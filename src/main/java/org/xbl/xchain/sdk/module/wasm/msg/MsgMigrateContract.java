package org.xbl.xchain.sdk.module.wasm.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
public class MsgMigrateContract extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    @JSONField(name = "contract_name")
    private String contractName;

    @JSONField(name = "wasm_byte_code")
    private byte[] wasmBytes;       //合约二进制

    @JSONField(name = "execute_perm")
    private String executePerm;     // format  (Org1&member||Org2,Org3&member,admin)

    private Integer policy;          // 1--白名单 2--黑名单

    @JSONField(name = "msg")
    private String migrateMsg;

    public MsgMigrateContract() {
    }

    @Override
    public String type() {
        return "wasm/MsgMigrateContract";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
