package org.xbl.xchain.sdk.module.iccp.types;

public enum AppChainOperationEnum {
    CROSS_APP_CHAIN_REGISTER(0),
    CROSS_APP_CHAIN_UPDATE(1),
    CROSS_APP_CHAIN_FREEZE(2),
    CROSS_APP_CHAIN_ACTIVATE(3),
    CROSS_APP_CHAIN_REVOKE(4);

    private int type;

    AppChainOperationEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
