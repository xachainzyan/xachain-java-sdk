package org.xbl.xchain.sdk.module.params.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 修改参数RestAPI反馈信息 实体类
 * @Author zhw9026
 * @Date 2021-02-24
 **/
@Data
@AllArgsConstructor
public class MsgModifyParameter extends Msg {

    //模块名
    private String subspace;

    //参数名
    private String key;

    //参数值
    private String value;

    //RestAPI信息发起者
    private String owner;

    public MsgModifyParameter() {
    }

    @Override
    public String type() {
        return null;
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
