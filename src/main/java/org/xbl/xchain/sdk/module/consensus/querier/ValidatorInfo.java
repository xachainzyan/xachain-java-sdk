package org.xbl.xchain.sdk.module.consensus.querier;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.module.consensus.types.Description;

@Data
public class ValidatorInfo {
    @JSONField(name = "operator_address")
    private String operatorAddress;
    @JSONField(name = "validator_address")
    private String validatorAddress;
    @JSONField(name = "consensus_pubkey")
    private String consensuspubkey;

    private Boolean jailed;

    private Integer status;

    @JSONField(name = "weight")
    private Integer power;

    private Description description;
}
