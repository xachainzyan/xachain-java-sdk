package org.xbl.xchain.sdk.module.member.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 *  新增账户RestAPI反馈信息 实体类
 *  @Author zhw9026
 *  @Date 2021-02-20
 */
@Data
@AllArgsConstructor
public class MsgChangeOrgAdmin extends Msg {

    private String orgFullId;
    private String newAdminAddress;
    private Integer isNwAdmin;
    private Integer power;
    @AminoFieldSerialize(format = "address")
    private String owner;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    public MsgChangeOrgAdmin() {
    }

    @Override
    public String type() {
        return "xchain/ChangeOrgAdmin";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }



}
