package org.xbl.xchain.sdk.module.member.types;

public enum PermissionPolicy {
    POLICY_ACCEPT(1, "白名单"),
    POLICY_DROP(2, "黑名单");

    private Integer policy;

    private String desc;

    public Integer getPolicy() {
        return policy;
    }

    PermissionPolicy(Integer policy, String desc) {
        this.policy = policy;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static  PermissionPolicy getPermissionPolicy(Integer policy){
        for (PermissionPolicy permissionPolicy:PermissionPolicy.values()) {
            if (permissionPolicy.getPolicy().equals(policy)) {
                return permissionPolicy;
            }
        }
        return null;
    }

}
