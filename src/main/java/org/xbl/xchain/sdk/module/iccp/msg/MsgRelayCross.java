package org.xbl.xchain.sdk.module.iccp.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.iccp.types.ICCP;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgRelayCross extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    private ICCP iccp;

    @AminoFieldSerialize(format = "time")
    private String timestamp;

    public MsgRelayCross() {
    }

    @Override
    public String type() {
        return "xchain/MsgRelayCross";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
