package org.xbl.xchain.sdk.module.wasm.types;

import lombok.Data;

@Data
public class ContractEvent {

    private String txHash;

    private String sender;

    private String contractName;

    private String key;

    private String value;

    public ContractEvent(String txHash, String sender, String contractName, String key, String value) {
        this.txHash = txHash;
        this.sender = sender;
        this.contractName = contractName;
        this.key = key;
        this.value = value;
    }

}
