package org.xbl.xchain.sdk.module.wasm.types;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class ContractMsg {

    private String contractName;

    private String functionName;

    private String paramJsonString;

    public ContractMsg(String contractName, String functionName, String paramJsonString) {
        this.contractName = contractName;
        this.functionName = functionName;
        this.paramJsonString = paramJsonString;
    }

    public <T> T getParam(Class<T> tClass) {
        return JSON.parseObject(this.paramJsonString, tClass);
    }

    public ContractMsg() {
    }
}
