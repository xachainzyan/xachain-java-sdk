package org.xbl.xchain.sdk.module.gov.types;

public enum ProposalStatusEnum {

    VOTING_PERIOD("VotingPeriod", "投票阶段"),
    EFFECTIV_EPERIOD("EffectivePeriod", "投票通过，待生效"),
    INVALID("Invalid", "投票不通过"),
    COMPLETED("Completed", "投票通过，已生效");
    private String status;

    private String desc;

    ProposalStatusEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
