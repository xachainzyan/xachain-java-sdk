package org.xbl.xchain.sdk.module.auth.msg;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;
import org.xbl.xchain.sdk.module.auth.types.Token;


/**
 * 转账信息 实体类
 * @Author zhw9026
 * @Date 2021-01-19
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
public class MsgTransfer extends Msg {

    @JSONField(name = "from_address")
    @AminoFieldSerialize(format = "address")
    private String fromAddress;

    @JSONField(name = "to_address")
    @AminoFieldSerialize(format = "address")
    private String toAddress;

    private Token[] amount;

    public MsgTransfer() {
    }

    @Override
    public String type() {
        return "xchain/MsgSend";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{fromAddress};
    }

}
