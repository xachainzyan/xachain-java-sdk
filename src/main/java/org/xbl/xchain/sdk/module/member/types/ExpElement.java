package org.xbl.xchain.sdk.module.member.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@Data
public class ExpElement {
    private String[] chainIds;
    private String[] orgExps;
    private String[] roleIds;
    private String[] addresses;
}
