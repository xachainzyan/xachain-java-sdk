package org.xbl.xchain.sdk.module.iccp.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class ICCP {

    private String src;
    private String dst;
    @JSONField(name = "cross_type")
    private int crossType;
    private Long sequence;
    private Long height;
    private byte[] proof;
    @JSONField(name = "initiator_info")
    private InitiatorInfo initiatorInfo;
    private Content content;
    private Response result;
    private String version;
    private byte[] extra;
}
