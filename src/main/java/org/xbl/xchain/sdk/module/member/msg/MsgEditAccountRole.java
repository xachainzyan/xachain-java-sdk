package org.xbl.xchain.sdk.module.member.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgEditAccountRole extends Msg {
    public enum EditType {
        REVOKEROLE(0L),
        ADDROLE(1L);

        private Long type;

        EditType(Long type) {
            this.type = type;
        }

        public Long getType() {
            return type;
        }
    }

    //被操作的账号地址
    private String address;

    //角色编号
    private String roleId;

    //操作类型
    private Long editType;

    @AminoFieldSerialize(format = "address")
    private String owner;


    public MsgEditAccountRole() {
    }

    @Override
    public String type() {
        return "xchain/EditAccountRole";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
