package org.xbl.xchain.sdk.module.wasm.querier;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ContractInfo {

    @JSONField(name = "code_hash")
    private String codeHash;
    @JSONField(name = "codeId")
    private String codeId;
    @JSONField(name = "contractAddr")
    private String contract_addr;
    private String creator;
    private String admin;
    @JSONField(name = "contract_name")
    private String contractName;
    @JSONField(name = "execute_perm")
    private String executePerm;

    private String language;

    private String label;

    private Integer version;

    private Integer policy;

}
