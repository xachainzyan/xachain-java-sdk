package org.xbl.xchain.sdk.module.consensus.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 修改Validator RestAPI反馈信息 实体类
 * @Author zhw9026
 * @Date 2021-03-09
 **/
@Data
@AllArgsConstructor
public class MsgEditValidator extends Msg {

    @AminoFieldSerialize(format = "address")
    private String validatorAddress;

    private Integer power;

    @AminoFieldSerialize(format = "address")
    private String owner;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    public MsgEditValidator() {
    }

    @Override
    public String type() {
        return "xchain/MsgEditValidator";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
