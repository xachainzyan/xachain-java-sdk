package org.xbl.xchain.sdk.module.iccp.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.module.cross.types.ValidatorSet;

@Data
public class FullCommit {

    @JSONField(name = "signed_header")
    private Object signedHeader;

    @JSONField(name = "validator_set")
    private ValidatorSet validatorSet;

    @JSONField(name = "next_validator_set")
    private ValidatorSet nextValidatorSet;
}
