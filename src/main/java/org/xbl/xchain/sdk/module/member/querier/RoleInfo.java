package org.xbl.xchain.sdk.module.member.querier;

import lombok.Data;

@Data
public class RoleInfo {

    private String roleId;
    private String baseRoleId;
    private String orgFullId;
    private String roleFullId;
    private Integer status;
}
