package org.xbl.xchain.sdk.module.wasm.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
public class MsgDestroyContract extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    @JSONField(name = "contract_name")
    private String contractName;


    public MsgDestroyContract() {
    }

    @Override
    public String type() {
        return "wasm/MsgDestroyContract";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
