package org.xbl.xchain.sdk.module.consensus.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.consensus.types.Description;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 创建Validator RestAPI反馈信息 实体类
 *
 * @Author zhw9026
 * @Date 2021-03-09
 **/
@Data
@AllArgsConstructor
public class MsgCreateValidator extends Msg {

    private Description description;

    @JSONField(name = "operator_address")
    @AminoFieldSerialize(format = "address")
    private String operatorAddress;

    //公钥
    @AminoFieldSerialize(format = "pubkey")
    private String pubkey;

    //权重
    private Integer power;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    public MsgCreateValidator() {
    }

    @Override
    public String type() {
        return "xchain/MsgCreateValidator";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{operatorAddress};
    }
}
