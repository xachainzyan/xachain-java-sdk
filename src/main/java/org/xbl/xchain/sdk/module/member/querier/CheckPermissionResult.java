package org.xbl.xchain.sdk.module.member.querier;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CheckPermissionResult {

    @JsonProperty("Ok")
    private Boolean ok;

    @JsonProperty("Cause")
    private String cause;
}
