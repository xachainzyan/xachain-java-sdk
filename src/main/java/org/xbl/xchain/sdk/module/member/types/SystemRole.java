package org.xbl.xchain.sdk.module.member.types;

public enum  SystemRole {
    NetworkOrgId("NetworkOrg"),
    RoleNwAdmin("networkAdmin"),
    RoleGateway("gateway"),
    RoleOrgAdmin("orgAdmin"),
    RolePeer("peer"),
    RoleCli("client"),
    RoleMember("member");

    private String role;

    SystemRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
