package org.xbl.xchain.sdk.module.cross.types;

import lombok.Data;

@Data
public class ValidatorSet {
    private Validator[] validators;
    private Validator proposer;
    private Long totalVotingPower;

}
