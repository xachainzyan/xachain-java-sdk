package org.xbl.xchain.sdk.module.iccp.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.iccp.types.ICCP;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgCross extends Msg {
    @AminoFieldSerialize(format = "address")
    private String sender;
    @AminoFieldSerialize(format = "time")
    private String timestamp;

    private ICCP iccp;

    public MsgCross() {
    }

    @Override
    public String type() {
        return "xchain/MsgCross";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
