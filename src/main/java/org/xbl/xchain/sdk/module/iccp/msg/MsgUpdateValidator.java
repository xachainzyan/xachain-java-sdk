package org.xbl.xchain.sdk.module.iccp.msg;

import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.iccp.types.FullCommit;
import org.xbl.xchain.sdk.types.Msg;

@Data
public class MsgUpdateValidator extends Msg {
    @AminoFieldSerialize(format = "address")
    private String sender;

    private FullCommit fullCommit;

    @Override
    public String type() {
        return "xchain/MsgUpdateValidator";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers()  {
        return new String[]{sender};
    }
}
