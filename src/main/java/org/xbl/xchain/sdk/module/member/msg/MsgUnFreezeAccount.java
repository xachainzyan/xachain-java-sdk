package org.xbl.xchain.sdk.module.member.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 撤销账号RestAPI反馈信息 实体类
 *
 * @Author zhw9026
 * @Date 2021-02-23
 **/
@Data
@AllArgsConstructor
public class MsgUnFreezeAccount extends Msg {

    //撤销的账号地址
    private String address;

    @AminoFieldSerialize(format = "address")
    private String owner;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    public MsgUnFreezeAccount() {
    }

    @Override
    public String type() {
        return "xchain/UnfreezeAccount";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }

}
