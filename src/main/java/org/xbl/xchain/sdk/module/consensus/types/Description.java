package org.xbl.xchain.sdk.module.consensus.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 创建Validator基本信息描述 RestAPI反馈信息 实体类
 * @Author zhw9026
 * @Date 2021-03-09
 **/
@Data
public class Description {

    //别名
    private String moniker;

    //身份认证
    private String identity;

    //网站
    private String website;

    //联系人
    @JSONField(name = "security_contact")
    private String securityContact;

    //其他描述
    private String details;

}
