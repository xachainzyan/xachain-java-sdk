package org.xbl.xchain.sdk.module.iccp.types;

public enum AppChainStateEnum {
    UNAVAILABLE(0,"unavailable"),
    AVAILABLE(1,"available"),
    FROZEN(2,"frozen"),
    FORBIDDEN(3,"forbidden"),
    REGISTERING(4,"registering"),
    UPDATING(5,"updating"),
    FREEZING(6,"freezing"),
    ACTIVATING(7,"activating"),
    REVOKING(8,"revoking");

    private int code;
    private String state;

    AppChainStateEnum(int code, String state) {
        this.code = code;
        this.state = state;
    }

    public int getCode() {
        return code;
    }

    public String getState() {
        return state;
    }

    public static String getStateByCode(int code) {
        for (AppChainStateEnum appChainStateEnum : AppChainStateEnum.values()){
            if (appChainStateEnum.code == code){
                return appChainStateEnum.getState();
            }
        }
        return "";
    }
}
