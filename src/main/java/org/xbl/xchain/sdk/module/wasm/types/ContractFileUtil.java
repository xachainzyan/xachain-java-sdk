package org.xbl.xchain.sdk.module.wasm.types;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ContractFileUtil {
    public static byte[] getContent(File file) throws IOException {
        long fileSize = file.length();
        if (fileSize > Integer.MAX_VALUE) {
            return null;
        }
        byte[] bytes = IOUtils.readFully(new FileInputStream(file), (int) fileSize);
        return bytes;
    }
}
