package org.xbl.xchain.sdk.module.gov.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgVote extends Msg {

    //提案编号
    @JSONField(name = "proposal_id")
    private String proposalId;

    @JSONField(name = "vote_type")
    private Integer voteType;

    @JSONField(name = "voter_address")
    @AminoFieldSerialize(format = "address")
    private String owner;

    public MsgVote() {
    }

    @Override
    public String type() {
        return "xchain/vote";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
