package org.xbl.xchain.sdk.module.gov.querier;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

@Data
public class ProposalInfo {

    @JSONField(name = "proposal_id")
    private String proposalId;

    @JSONField(name = "router_key")
    private String routerKey;

    @JSONField(name = "op_type")
    private String opType;

    private String content;

    private List<Admins> admins;

    @JSONField(name = "approved_admins")
    private List<VoteInfo> approvedAdmins;

    @JSONField(name = "refused_admins")
    private List<VoteInfo> refusedAdmins;

    @JSONField(name = "abstained_admins")
    private List<VoteInfo> abstainedAdmins;

    @JSONField(name = "create_time")
    private String createTime;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    @JSONField(name = "proposal_status")
    private String proposalStatus;

    @JSONField(name = "propose_level")
    private Integer proposeLevel;

    @Data
    public class Admins {
        private String address;
        private Long power;
    }
    @Data
    public class VoteInfo {
        @JSONField(name = "voter_address")
        private String voterAddress;
        @JSONField(name = "vote_type")
        private String voteType;
        @JSONField(name = "vote_time")
        private String voteTime;
        private Long power;
    }
}
