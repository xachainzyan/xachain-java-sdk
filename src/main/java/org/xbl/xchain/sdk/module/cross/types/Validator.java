package org.xbl.xchain.sdk.module.cross.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;

@Data
public class Validator {

    @AminoFieldSerialize(format = "address")
    private String address;

    @JSONField(name = "pub_key")
    @AminoFieldSerialize(format = "pubkey")
    private String pubKey;

    @JSONField(name = "voting_power")
    private Long votingPower;

    @JSONField(name = "proposer_priority")
    private Long proposerPriority ;
}
