package org.xbl.xchain.sdk.module.member.querier;

import lombok.Data;
import org.xbl.xchain.sdk.module.member.types.ExpElement;

@Data
public class PermissionInfo {

    private String resource;

    private String ownerAddress;

    private ExpElement[] expElements;

    private String contractAdmin;

    private PermissionInfo pendingPermission;

    private Integer policy;

    private Integer status;

}
