package org.xbl.xchain.sdk.module.member.querier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfo {

    private String address;
    private String orgId;
    private List<AccountRole> accountRoles;
    private Integer status;
    private Integer preRevokeStatus;
    private Integer power;

    @Data
    public class AccountRole {
        private String roleId;
        private Integer status;
    }
}
