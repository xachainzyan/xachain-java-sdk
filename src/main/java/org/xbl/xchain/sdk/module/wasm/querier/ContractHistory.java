package org.xbl.xchain.sdk.module.wasm.querier;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;

@Data
public class ContractHistory {
    private String operation;

    @JSONField(name = "code_id")
    private Integer codeId;

    private AbsoluteTxPosition updated;

    @AminoFieldSerialize(format = "string_to_bytes")
    private JSONObject msg;
}
