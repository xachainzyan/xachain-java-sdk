package org.xbl.xchain.sdk.module.cross.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.cross.types.RelayValidators;
import org.xbl.xchain.sdk.types.Msg;

@Data
public class MsgRelayValidators extends Msg {
    @AminoFieldSerialize(format = "address")
    private String sender;

    @JSONField(name = "relay_validators")
    private RelayValidators relayValidators;

    @AminoFieldSerialize(format = "time")
    private String timestamp;

    @Override
    public String type() {
        return "xchain/MsgRelayValidators";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
