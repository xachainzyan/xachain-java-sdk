package org.xbl.xchain.sdk.module.wasm.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;
import org.xbl.xchain.sdk.module.auth.types.Token;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonPropertyOrder(alphabetic = true)
@Data
@AllArgsConstructor
public class MsgExecuteContract extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    @AminoFieldSerialize(format = "address")
    private String origin;

    @JSONField(name = "contract_name")
    private String contractName;

    @JSONField(name = "msg")
    private String args;

    @JSONField(name = "sent_funds")
    private Token[] sentFunds;

    public MsgExecuteContract() {
    }

    public MsgExecuteContract(String contractName, String sender, String args) {
        super();
        this.contractName = contractName;
        this.sender = sender;
        this.origin = sender;
        this.args = args;
        this.sentFunds = new Token[]{};
    }

    @Override
    public String type() {
        return "wasm/MsgExecuteContract";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }

}