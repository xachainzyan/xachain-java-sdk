package org.xbl.xchain.sdk.module.iccp.types;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class Content {
    private int category;
    @JSONField(name = "callback_id")
    private byte[] callbackId;
    private byte[] data;

}
