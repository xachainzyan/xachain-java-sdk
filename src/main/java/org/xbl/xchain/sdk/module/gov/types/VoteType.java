package org.xbl.xchain.sdk.module.gov.types;

public enum VoteType {
    PASS(0), NOPASS(1), ABSTENTION(2);

    private Integer type;

    VoteType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public static VoteType getVoteType(Integer type){
        for (VoteType voteType : VoteType.values()) {
            if (voteType.getType().equals(type)) {
                return voteType;
            }
        }
        return null;
    }
}
