package org.xbl.xchain.sdk.module.member.querier;

import lombok.Data;

import java.util.List;

@Data
public class OrgInfo {

    private String orgId;
    private String orgFullId;
    private Integer level;
    private String orgAdminAddress;
    private List<String> subOrgIds;
    private List<String> orgAccounts;
    private List<String> orgRoleIds;
    private String parentId;
    private String ultParent;
    private Integer status;
    private Integer preRevokeStatus;
}
