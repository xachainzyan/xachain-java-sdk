package org.xbl.xchain.sdk.module.member.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

/**
 * 新增账户RestAPI反馈信息 实体类
 *
 * @Author zhw9026
 * @Date 2021-02-20
 */
@Data
public class MsgAddAccount extends Msg {

    //账号信息
    private String address;

    //组织编号
    private String orgFullId;

    //角色编号
    private String roleId;

    @AminoFieldSerialize(format = "address")
    private String owner;

    public MsgAddAccount() {
    }

    public MsgAddAccount(String address, String orgFullId, String roleId, String owner) {
        this.address = address;
        this.orgFullId = orgFullId;
        this.roleId = roleId;
        this.owner = owner;
    }

    @Override
    public String type() {
        return "xchain/AddAccount";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
