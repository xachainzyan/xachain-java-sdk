package org.xbl.xchain.sdk.module.celerain.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgCelerain extends Msg {

    private String key;

    private String value;

    @AminoFieldSerialize(format = "address")
    private String owner;

    public MsgCelerain() {
    }

    @Override
    public String type() {
        return "celerain/MsgCelerain";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }
}
