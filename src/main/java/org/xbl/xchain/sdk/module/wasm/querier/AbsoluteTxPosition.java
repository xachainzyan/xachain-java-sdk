package org.xbl.xchain.sdk.module.wasm.querier;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class AbsoluteTxPosition {
    @JSONField(name = "BlockHeight")
    private Integer blockHeight;

    @JSONField(name = "TxIndex")
    private Integer txIndex;
}
