package org.xbl.xchain.sdk.module.cross.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgCrossFromOrigin extends Msg {

    @AminoFieldSerialize(format = "address")
    private String sender;

    private String destination;

    @JSONField(name = "msg_type")
    private String msgType;

    @JSONField(name = "msg_bytes")
    private byte[] msgBytes;

    @JSONField(name = "callback_id")
    private byte[] callbackId;

    @JSONField(name = "callback_bytes")
    private byte[] callbackBytes;

    @AminoFieldSerialize(format = "time")
    private String timestamp;

    public MsgCrossFromOrigin() {
    }

    @Override
    public String type() {
        return "xchain/MsgCrossFromOrigin";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
