package org.xbl.xchain.sdk.module.iccp.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.module.iccp.types.AppChainInfo;
import org.xbl.xchain.sdk.types.Msg;

@Data
@AllArgsConstructor
public class MsgAppChain extends Msg {

    private String role;
    @AminoFieldSerialize(format = "address")
    private String sender;

    private AppChainInfo info;

    private int operation;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;


    public MsgAppChain() {
    }

    @Override
    public String type() {
        return "xchain/MsgAppChain";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{sender};
    }
}
