package org.xbl.xchain.sdk.module.auth.types;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Token {

    private String denom;

    private String amount;
}
