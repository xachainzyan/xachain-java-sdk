package org.xbl.xchain.sdk.module.member.msg;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xbl.xchain.sdk.amino.AminoFieldSerialize;
import org.xbl.xchain.sdk.types.Msg;
import org.xbl.xchain.sdk.module.member.types.ExpElement;

/**
 * 新增账户RestAPI反馈信息 实体类
 *
 * @Author zhw9026
 * @Date 2021-02-20
 */
@Data
@AllArgsConstructor
public class MsgSavePermission extends Msg {

    //账号信息
    private String resource;

    private Integer policy;

    private ExpElement[] expElements;

    @AminoFieldSerialize(format = "address")
    private String owner;

    @JSONField(name = "effective_time")
    private Long effectiveTime;

    @JSONField(name = "vote_end_time")
    private Long voteEndTime;

    public MsgSavePermission() {
    }

    @Override
    public String type() {
        return "xchain/SavePermission";
    }

    @Override
    public Exception ValidateBasic() {
        return null;
    }

    @Override
    public String[] signers() {
        return new String[]{owner};
    }

}
