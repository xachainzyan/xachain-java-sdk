package org.xbl.xchain.sdk.amino;

import org.junit.Test;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.types.KeyInfo;

public class AminoTest {

    @Test
    public void testAmino() throws Exception {
        XchainClient xchainClient = new XchainClient();
        KeyInfo keyInfo = xchainClient.generateKeyInfo(AlgorithmType.SM2, "xchain");
//        BytesInterface pubKeyBytes = keyInfo.getPubKeyBytes();
//        byte[] bytes = Codec.getAmino().marshalBinaryBare(keyInfo.getPubKeyBytes());
//        BytesInterface pubKeyBytes1 = Codec.getAmino().unmarshalBinaryBare(bytes, BytesInterface.class);
        T1 t1 = new T1("123", keyInfo.getAddress(), keyInfo.getConsensusPubKey());
        byte[] bytes = Codec.getAmino().marshalBinaryBare(t1);
        T1 t11 = Codec.getAmino().unmarshalBinaryBare(bytes, T1.class);
        System.out.println(1);

    }
}
