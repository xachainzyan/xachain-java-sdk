package org.xbl.xchain.sdk.amino;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class T1 {

    private String desc;

    @AminoFieldSerialize
    private String address;

    @AminoFieldSerialize(format = "pubkey")
    private String pubkey;


}
