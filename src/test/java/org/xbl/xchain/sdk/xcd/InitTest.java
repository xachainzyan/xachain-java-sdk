//package org.xbl.xchain.sdk.xcd;
//
//import org.apache.commons.lang.StringUtils;
//import org.junit.Test;
//import org.xbl.xchain.sdk.SysConfig;
//import org.xbl.xchain.sdk.amino.BytesInterface;
//import org.xbl.xchain.sdk.crypto.algo.Algorithm;
//import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
//import org.xbl.xchain.sdk.types.Address;
//import org.xbl.xchain.sdk.types.KeyInfo;
//import org.xbl.xchain.sdk.utils.PubkeyUtil;
//import org.xbl.xchain.sdk.xcd.init.InitConfig;
//import org.xbl.xchain.sdk.xcd.init.baseconfig.*;
//import org.xbl.xchain.sdk.xcd.init.utils.GenesisUtils;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.text.ParseException;
//import java.util.Arrays;
//import java.util.Base64;
//import java.util.Calendar;
//import java.util.Date;
//
//
//public class InitTest {
//    public static String moniker="test";
//    public static String xchainId="testxchain";
//    public static boolean isFirstNode=true;
//    public static String testPath = "/home/rootroot/newspace/xchain";
//    public static AlgorithmType cryptoType=AlgorithmType.SM2;
//    public static void WriteStringToFile(String filePath,String FileStr) {
//        try {
//            FileWriter fw = new FileWriter(filePath, false);
//            BufferedWriter bw = new BufferedWriter(fw);
//            bw.write(FileStr);
//
//            bw.close();
//            fw.close();
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
//    /**
//     * 先根遍历序递归删除文件夹
//     *
//     * @param dirFile 要被删除的文件或者目录
//     * @return 删除成功返回true, 否则返回false
//     */
//    public static boolean deleteFile(File dirFile) {
//        // 如果dir对应的文件不存在，则退出
//        if (!dirFile.exists()) {
//            return true;
//        }
//
//        if (dirFile.isFile()) {
//            return dirFile.delete();
//        } else {
//
//            for (File file : dirFile.listFiles()) {
//                deleteFile(file);
//            }
//        }
//
//        return dirFile.delete();
//    }
//
//    @Test
//    public void testGenerate() throws ParseException {
////        String str = "AIDDrVM2b81+W3+XAMeI3gJUNkmeKB4ZJ66HyJ1aV8bd";
////        System.out.println(str.length());
////        byte[] nodeKeyByte = Base64.getDecoder().decode(str);
////        System.out.println(nodeKeyByte.length);
////        System.out.print(new String(nodeKeyByte));
////        SM2General sm2 = new SM2General();
////        SM2KeyPair sm2KeyPair = sm2.generateKeyPair();
////        System.out.println(sm2KeyPair.getPrivateKey().toByteArray().length);
////        System.out.println(Crypto.generateMnemonic());
//
//        Calendar calendar=Calendar.getInstance();
////        // 2、取得时间偏移量：
////        int zoneOffset = calendar.get(java.util.Calendar.ZONE_OFFSET);
////        // 3、取得夏令时差：
////        int dstOffset = calendar.get(java.util.Calendar.DST_OFFSET);
////        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：
////        calendar.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
//        Date date = new Date();
//        System.out.println(date);
////        calendar.add(Calendar.HOUR,-8);
//        String utcstr = GenesisUtils.UTCDateFormat.format(date);
//        String localstr = GenesisUtils.DefaultSimpleDateFormat.format(date);
//        System.out.println(utcstr);
//        System.out.println(localstr);
//        Date date1 = GenesisUtils.UTCDateFormat.parse(utcstr);
//        Date date2 = GenesisUtils.DefaultSimpleDateFormat.parse(utcstr);
//        System.out.println(date1);
//        System.out.println(date2);
//        System.out.println(GenesisUtils.UTCDateFormat.format(date1));
//
//
////        try {
////            Date mydate = GenesisUtils.DefaultSimpleDateFormat.parse(timestr);
////            System.out.println(GenesisUtils.DefaultSimpleDateFormat.format(mydate));
////        } catch (ParseException e) {
////            System.out.println("aaaaa");
////        }
//    }
//
//    @Test
//    public void testInit() {
//
//        XcdClient xcdClient = new XcdClient(xchainId,BaseConfig.getDefaultMoniker(), cryptoType);
//        String jackMeno= "want knife response awesome ball village eagle oppose decline giraffe giggle grit neck someone travel spray build course riot season vacant sadness assume future";
//        String org1Admin = "marriage key nose sort injury exotic banana raccoon rug round network fan diamond cloth mesh rabbit asset endorse crunch light south august siren spread";
//        try{
//            String nodeKey = xcdClient.generatePrikey();
//            String privVateKey = xcdClient.generatePrikey();
//
//            xcdClient.withNodeKey(new NodeKey(nodeKey,cryptoType));
//            xcdClient.withPrivValidatorKey(new PrivValidatorKey(privVateKey,cryptoType));
//
//            InitConfig initConfig = xcdClient.initXChain(isFirstNode);
//            String genesisJson=new String(initConfig.getGenesisJsonFile());
//            WriteStringToFile("/home/rootroot/newspace/xchain/testFiles/genesis.json",genesisJson);
//            genesisJson = xcdClient.addGenesisAccount(genesisJson,jackMeno, 1000000000000L);
//            genesisJson = xcdClient.addGenesisAdmin(genesisJson,jackMeno);
//            genesisJson = xcdClient.addGenesisOrg(genesisJson,"org1",org1Admin);
//            String genTxJson = xcdClient.generateGenesisTx(jackMeno);
//            genesisJson = xcdClient.collectionGenTx(genesisJson,genTxJson,genTxJson);
//
//            File file =new File(testPath+"/testFiles/");
//            if  (!file.exists()  && !file.isDirectory()) {
//                file.mkdirs();
//            }
//
//            WriteStringToFile(testPath+"/testFiles/app.toml",new String(initConfig.getAppTomlFile()));
//            WriteStringToFile(testPath+"/testFiles/config.toml",new String(initConfig.getConfigTomlFile()));
//            WriteStringToFile(testPath+"/testFiles/genesis2.json",genesisJson);
//            WriteStringToFile(testPath+"/testFiles/node_key.json",initConfig.getNodeKey().toJsonString());
//            WriteStringToFile(testPath+"/testFiles/priv_validator_key.json",initConfig.getPrivValidatorKey().toJsonString());
//            WriteStringToFile(testPath+"/testFiles/priv_validator_state.json",new String(initConfig.getPrivValidatorStateJson()));
//            WriteStringToFile(testPath+"/testFiles/xccli_config.toml",new String(initConfig.getXccliConfigTomlFile()));
//
//        }catch (Exception e){
//            System.out.println("err :" + e.getMessage());
//        }
//    }
//
//    @Test
//    public void testTwoOrgInit(){
//        XcdClient xcdClient = new XcdClient(xchainId,BaseConfig.getDefaultMoniker(), AlgorithmType.SM2);
//        String netAdmin1= "palace actual finger parrot drop famous sustain shine present purse slam skate witness left harsh govern aerobic guide pottery antique goat develop salmon light";
//        String org1Admin = "medal erosion neck dizzy stamp nominee razor window thumb balcony soap film ceiling once expose swap muffin mimic jacket vendor gauge lunch syrup mule";
//        String netAdmin2= "want knife response awesome ball village eagle oppose decline giraffe giggle grit neck someone travel spray build course riot season vacant sadness assume future";
//        String org2Admin = "marriage key nose sort injury exotic banana raccoon rug round network fan diamond cloth mesh rabbit asset endorse crunch light south august siren spread";
//        try{
//            xcdClient.withMoniker("node2");
//            InitConfig initConfigOrg2 = xcdClient.initXChain(isFirstNode);
//            xcdClient.withMoniker(BaseConfig.getDefaultMoniker());
//            String genesisJson2=new String(initConfigOrg2.getGenesisJsonFile());
//            genesisJson2 = xcdClient.addGenesisAccount(genesisJson2,netAdmin2, 1000000000000L);
//            String genTxOrg2 = xcdClient.generateGenesisTx(netAdmin2);
//
//            xcdClient.withMoniker("node1");
//            InitConfig initConfigOrg1 = xcdClient.initXChain(isFirstNode);
//            xcdClient.withMoniker(BaseConfig.getDefaultMoniker());
//            String genesisJson1=new String(initConfigOrg1.getGenesisJsonFile());
//            genesisJson1 = xcdClient.addGenesisAccount(genesisJson1,netAdmin1, 1000000000000L);
////            genesisJson1 = xcdClient.addGenesisAccount(genesisJson1,netAdmin2, 1000000000000L);
//            genesisJson1 = xcdClient.addGenesisAdmin(genesisJson1,netAdmin1);
//            genesisJson1 = xcdClient.addGenesisAdmin(genesisJson1,netAdmin2);
//
//            genesisJson1 = xcdClient.addGenesisOrg(genesisJson1,"org1",org1Admin);
//            xcdClient.withMoniker("node2");
//            xcdClient.withNodeKey(initConfigOrg2.getNodeKey());
//            genesisJson1 = xcdClient.addGenesisOrg(genesisJson1,"org2",org2Admin);
//            xcdClient.withMoniker("node1");
//
//            xcdClient.withNodeKey(initConfigOrg1.getNodeKey());
//            String genTxOrg1 = xcdClient.generateGenesisTx(netAdmin1);
//
//            genesisJson1 = xcdClient.collectionGenTx(genesisJson1,new String[]{genTxOrg1,genTxOrg2});
//
//            String persistent1 = NodeKey.getNode_id(initConfigOrg1.getNodeKey().getPriv_key().getValue()).toLowerCase()+"@192.168.137.140:26656";
//            String persistent2 = NodeKey.getNode_id(initConfigOrg2.getNodeKey().getPriv_key().getValue()).toLowerCase()+"@192.168.137.140:26657";
//
//            ConfigTomlMap configTomlMap1= new ConfigTomlMap(new String(initConfigOrg1.getConfigTomlFile()));
//            P2P p1 = (P2P) configTomlMap1.getObjectByKey(ConfigTomlMap.TomlP2P);
//            p1.setPersistentPeers(persistent2);
//            configTomlMap1.setObject2Map(p1);
//            initConfigOrg1.setConfigTomlFile(configTomlMap1.toTomlString().getBytes());
//
//            ConfigTomlMap configTomlMap2= new ConfigTomlMap(new String(initConfigOrg2.getConfigTomlFile()));
//
////            P2P p2 = (P2P) configTomlMap2.getObjectByKey(ConfigTomlMap.TomlP2P);
////            p2.setPersistentPeers(persistent1);
////            configTomlMap2.setObject2Map(p2);
////            initConfigOrg2.setConfigTomlFile(configTomlMap2.toTomlString().getBytes());
//
//
//            System.out.println("xcd start --p2p.persistent_peers="+persistent1+","+persistent2);
//            WriteStringToFile(testPath+"/testTwoOrg/orgs","2");
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/config/app.toml",new String(initConfigOrg1.getAppTomlFile()));
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/config/config.toml",new String(initConfigOrg1.getConfigTomlFile()));
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/config/genesis.json",genesisJson1);
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/config/node_key.json",initConfigOrg1.getNodeKey().toJsonString());
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/config/priv_validator_key.json",initConfigOrg1.getPrivValidatorKey().toJsonString());
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xcd/data/priv_validator_state.json",new String(initConfigOrg1.getPrivValidatorStateJson()));
//            WriteStringToFile(testPath+"/testTwoOrg/node1/.xccli/config/config.toml",new String(initConfigOrg1.getXccliConfigTomlFile()));
//
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/config/app.toml",new String(initConfigOrg2.getAppTomlFile()));
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/config/config.toml",new String(initConfigOrg2.getConfigTomlFile()));
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/config/genesis.json",genesisJson1);
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/config/node_key.json",initConfigOrg2.getNodeKey().toJsonString());
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/config/priv_validator_key.json",initConfigOrg2.getPrivValidatorKey().toJsonString());
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xcd/data/priv_validator_state.json",new String(initConfigOrg2.getPrivValidatorStateJson()));
//            WriteStringToFile(testPath+"/testTwoOrg/node2/.xccli/config/config.toml",new String(initConfigOrg2.getXccliConfigTomlFile()));
//
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testOrgsInit(){
//        int orgs = 3;
//        int root = 0;
//        System.out.println(deleteFile(new File(testPath+"/testTwoOrg")));
//
//        XcdClient xcdClient = new XcdClient(xchainId,BaseConfig.getDefaultMoniker(), AlgorithmType.SM2);
//        String[] netAdmins = new String[orgs];
//        String[] orgAdmins = new String[orgs];
//        String[] genTxs = new String[orgs];
//        InitConfig[] initConfigs = new InitConfig[orgs];
//
//        for (int i = 0 ; i < orgs ; i++){
////            netAdmins[i]=Crypto.generateMnemonic();
////            netAdmins[0]="sand duck knife lunch regret leisure flavor soldier record motion blossom mule obtain high spawn kind stove often alter vault general clip useless pencil";
////            orgAdmins[i]=Crypto.generateMnemonic();
////            orgAdmins[0]="powder then bicycle peasant blue absurd pool exotic door crop viable merge have stable test person crawl please exit scan mistake skate supply hunt";
////            System.out.println("netAdmins"+i+"     "+xcdClient.getKey(netAdmins[i]).getAddress());
////            System.out.println("orgAdmins"+i+"     "+xcdClient.getKey(orgAdmins[i]).getAddress());
//        }
//        try{
//            for (int i = 0 ; i < orgs ; i++){
//                xcdClient = new XcdClient(xchainId,"node"+(i+1), AlgorithmType.SM2);
//                initConfigs[i] = xcdClient.initXChain(isFirstNode);
//                genTxs[i] = xcdClient.generateGenesisTx(netAdmins[i]);
//            }
//
//            String genesisJson1=new String(initConfigs[0].getGenesisJsonFile());
//            for (int i = 0 ; i < orgs ; i++){
//                genesisJson1 = xcdClient.addGenesisAccount(genesisJson1,netAdmins[i], 1000000000000L);
//                genesisJson1 = xcdClient.addGenesisAdmin(genesisJson1,netAdmins[i]);
//            }
//            String[] persistents = new String[orgs];
//            for (int i = 0 ; i < orgs ; i++){
//                xcdClient.withMoniker("node"+(i+1));
//                xcdClient.withNodeKey(initConfigs[i].getNodeKey());
//                xcdClient.withPrivValidatorKey(initConfigs[i].getPrivValidatorKey());
//                genesisJson1 = xcdClient.addGenesisOrg(genesisJson1,"org"+(i+1),orgAdmins[i]);
//                persistents[i] = NodeKey.getNode_id(initConfigs[i].getNodeKey().getPriv_key().getValue()).toLowerCase()+"@192.168.137.132:70"+(11+i);
//            }
//
//            genesisJson1 = xcdClient.collectionGenTx(genesisJson1,genTxs);
//
//            for (int i = 0 ; i < orgs ; i++){
//                ConfigTomlMap configTomlMap= new ConfigTomlMap(new String(initConfigs[i].getConfigTomlFile()));
//                P2P p = (P2P) configTomlMap.getObjectByKey(ConfigTomlMap.TomlP2P);
//                if (i == root){
//                    p.setPersistentPeers(StringUtils.join(persistents, ","));
//                }else{
//                    p.setPersistentPeers(persistents[root]);
//                }
//                p.setLaddr("tcp://0.0.0.0:70"+(11+i));
//                p.setPersistentPeers(StringUtils.join(persistents, ","));
//                configTomlMap.setObject2Map(p);
//
//                Consensus consensus = (Consensus) configTomlMap.getObjectByKey(ConfigTomlMap.TomlConsensus);
//                consensus.setCreateEmptyBlocks(true);
//                consensus.setCreateEmptyBlocksInterval("5m0s");
//                configTomlMap.setObject2Map(consensus);
//
//                initConfigs[i].setConfigTomlFile(configTomlMap.toTomlString().getBytes());
//                System.out.println("node"+(i+1)+"      "+persistents[i]);
//            }
//
//            System.out.println("xcd start --p2p.persistent_peers="+StringUtils.join(persistents, ","));
//
//            for (int i = 0 ; i < orgs ; i++){
//                String filePath = testPath+"/testTwoOrg/node"+(i+1)+"/.xcd/config";
//                File file =new File(filePath);
//                if  (!file.exists()  && !file.isDirectory()) {
//                    file.mkdirs();
//                }
//                WriteStringToFile(filePath+"/app.toml",new String(initConfigs[i].getAppTomlFile()));
//                WriteStringToFile(filePath+"/config.toml",new String(initConfigs[i].getConfigTomlFile()));
//                WriteStringToFile(filePath+"/genesis.json",genesisJson1);
//                WriteStringToFile(filePath+"/node_key.json",initConfigs[i].getNodeKey().toJsonString());
//                WriteStringToFile(filePath+"/priv_validator_key.json",initConfigs[i].getPrivValidatorKey().toJsonString());
//                filePath = testPath+"/testTwoOrg/node"+(i+1)+"/.xcd/data";
//                file =new File(filePath);
//                if  (!file.exists()  && !file.isDirectory()) {
//                    file.mkdirs();
//                }
//                WriteStringToFile(filePath+"/priv_validator_state.json",new String(initConfigs[i].getPrivValidatorStateJson()));
//                filePath = testPath+"/testTwoOrg/node"+(i+1)+"/.xccli/config";
//                file =new File(filePath);
//                if  (!file.exists()  && !file.isDirectory()) {
//                    file.mkdirs();
//                }
//                WriteStringToFile(filePath+"/config.toml",new String(initConfigs[i].getXccliConfigTomlFile()));
//            }
//            WriteStringToFile(testPath+"/testTwoOrg/orgs",orgs+"");
//            WriteStringToFile(testPath+"/testTwoOrg/root",(root+1)+"");
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testMeno() throws Exception {
//        String jackMeno= "want knife response awesome ball village eagle oppose decline giraffe giggle grit neck someone travel spray build course riot season vacant sadness assume future";
//        String org1Admin = "marriage key nose sort injury exotic banana raccoon rug round network fan diamond cloth mesh rabbit asset endorse crunch light south august siren spread";
//
//        String jack = "spatial venue diet answer jacket ready practice vocal someone match farm betray taxi promote social ensure north raw marble light clever concert sauce hood";
//        jack = jackMeno;
//        KeyInfo keyInfo = new KeyInfo(jack, AlgorithmType.SM2, new SysConfig().getMainPrefix());
//        Algorithm algorithm = keyInfo.getAlgorithm();
//        keyInfo.setPrivateKey(algorithm.genPrivateKeyFromMnemonic(jack));
//        keyInfo.setPublicKey(algorithm.genPublicKey(keyInfo.getPrivateKey()));
//        keyInfo.setAddress(algorithm.genAddressFromPublicKey(Address.Bech32MainPrefix, keyInfo.getPublicKey()));
//        System.out.println(keyInfo.getAddress());
//        System.out.println(PubkeyUtil.Bech32ifyPubKey(Address.GetBech32ConsensusPubPrefix(),new BytesInterface(algorithm.parsePubKey(keyInfo.getPublicKey()), algorithm.getType().getPubType())));
//    }
//
//    @Test
//    public void testNodeKey() {
//        String str = "1ulx45dfpqvh6nwkgtjs0vfssfxkj63g0svlcusrc9nylw2vc5kwsxxhhj96ak575ndr";
//
//        System.out.println(new String(Base64.getDecoder().decode(str)));
//
//        String str1 = "1ulx45dfpqvh6nwkgtjs0vfssfxkj63g0svlcusrc9ny";
//        System.out.println(Base64.getDecoder().decode(str1).length);
//
//        String str2 = "Ay+pushcoPYmEEmtLUUPgz+OQHgsyfcpmKWdAxr3kXXb";
//        System.out.println((Arrays.equals(Base64.getDecoder().decode(str1), Base64.getDecoder().decode(str2))));
//
//        String str3 = "tendermint/PubKeySm2";
//        System.out.println(str3.getBytes().length);
//        System.out.println("1ulx45dfpqw0p8t5fe49ertpevh9t9rysyafwskh6qd6pfhv576acyxcc3aeesdytk9l".length());
//        System.out.println(("1uuze8hew0z2ekgsl2kz774uej3gn5cp2tmngea").length());
//        System.out.println(("C529E8B77EEC0BDE564DD380844E9B99F66FA5CD").length());
//        System.out.println(("037A0DAF59B442D2EBDA3C3D529BA89E0095232442008637FCC4F4F7A0293C6B3D").length());
//    }
//    @Test
//    public void testConfigToml() {
//        ConfigTomlMap configTomlMap = new ConfigTomlMap();
//        configTomlMap.setDefaultConfigToml();
//        TxIndex txIndex = (TxIndex) configTomlMap.getObjectByKey(ConfigTomlMap.TomlTxIndex);
//        System.out.println(txIndex.getIndexer());
//        System.out.println(txIndex.getIndexKeys());
//        System.out.println(txIndex.isIndexAllKeys());
////
////        configTomlMap.setObject2Map(txIndex);
////
////        Fastsync fastsync = (Fastsync) configTomlMap.getObjectByKey(ConfigTomlMap.TomlFastsync);
////        System.out.println(fastsync.getVersion());
////
////        Consensus consensus = (Consensus) configTomlMap.getObjectByKey(ConfigTomlMap.TomlConsensus);
////        System.out.println(consensus.getPeerGossipSleepDuration());
////        System.out.println(consensus.getCreateEmptyBlocksInterval());
////
////        RPC rpc = (RPC) configTomlMap.getObjectByKey(ConfigTomlMap.TomlRPC);
////        System.out.println(rpc.getMaxOpenConnections());
////        System.out.println(Arrays.toString(rpc.getCorsAllowedMethods()));
//
//
////        TomlWriter tomlWriter = new TomlWriter();
////        ConfigTomlMap configTomlMap1 = new ConfigTomlMap(tomlWriter.write(configTomlMap.getObj2ConfigMap()));
////        TxIndex txIndex1 = (TxIndex) configTomlMap1.getObjectByKey(ConfigTomlMap.TomlTxIndex);
////        System.out.println(txIndex1.getIndexer());
////        System.out.println(txIndex1.getIndexKeys());
////        System.out.println(txIndex1.isIndexAllKeys());
//
//        BaseConfig baseConfig = (BaseConfig) configTomlMap.getObjectByKey(ConfigTomlMap.TomlBaseConfig);
//        System.out.println(baseConfig.genesisFile());
//        System.out.println(baseConfig.getDbBackend());
//
//    }
//}
