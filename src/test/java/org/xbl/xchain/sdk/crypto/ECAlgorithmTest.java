package org.xbl.xchain.sdk.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.pkcs.jcajce.JcaPKCS8EncryptedPrivateKeyInfoBuilder;
import org.junit.jupiter.api.Test;
import org.xbl.xchain.sdk.crypto.algo.*;
import org.xbl.xchain.sdk.crypto.sm.SM2General;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

class ECAlgorithmTest {

    @Test
    void verifyPubKeyLength() throws Exception {
        System.out.println("----");
        Algorithm[] algorithms = new Algorithm[]{Secp256k1.getInstance(), SM2.getInstance(), Ed25519.getInstance()};
        for(Algorithm algorithm : algorithms){
            System.out.println("  -----  " + algorithm.getType().name() + "  -----  ");
            String mnemonic = Mnemonic.generateMnemonic();
            PrivateKey bcecPrivateKey2 = null;
            bcecPrivateKey2 = algorithm.genPrivateKeyFromMnemonic(mnemonic);
            PublicKey publicKey = algorithm.genPublicKey(bcecPrivateKey2);
            String address = algorithm.genAddressFromPublicKey("xchain", publicKey);
            System.out.println(address);
        }
    }

    @Test
    void signAndVerify() throws Exception {
        System.out.println("----");
        Algorithm[] algorithms = new Algorithm[]{Secp256k1.getInstance()};
        for(Algorithm algorithm : algorithms){
            System.out.println("  -----  " + algorithm.getType().name() + "  -----  ");
            String mnemonic = Mnemonic.generateMnemonic();
            System.out.println(mnemonic);
            PrivateKey bcecPrivateKey2 =  algorithm.genPrivateKeyFromMnemonic(mnemonic);
            byte[] sign = algorithm.sign(bcecPrivateKey2, "Hello world!".getBytes(StandardCharsets.UTF_8));
            System.out.println(Hex.encodeHex(sign));
            PublicKey publicKey = algorithm.genPublicKey(bcecPrivateKey2);
            System.out.println(SM2General.printHexString(publicKey.getEncoded()));
            boolean b = algorithm.verifySignature(publicKey, "Hello world!".getBytes(StandardCharsets.UTF_8), sign);
            System.out.println(b);
            String address = algorithm.genAddressFromPublicKey("xchain", publicKey);
            System.out.println(address);
        }

        for(Algorithm algorithm : algorithms){
            System.out.println("  -----  " + algorithm.getType().name() + "  -----  ");
            String mnemonic = Mnemonic.generateMnemonic();
            System.out.println(mnemonic);
            AsymmetricKeyParameter privateParameter =  algorithm.genPrivateKeyParameterFromMnemonic(mnemonic);
            byte[] sign = algorithm.sign(privateParameter, "Hello world!".getBytes(StandardCharsets.UTF_8));
            System.out.println(Hex.encodeHex(sign));
            AsymmetricKeyParameter publicParameter = algorithm.genPubKeyParameter(privateParameter);
            boolean b = algorithm.verifySignature(publicParameter, "Hello world!".getBytes(StandardCharsets.UTF_8), sign);
            System.out.println(b);
        }

    }

    @Test
    void someBouncyEd25519Test() throws Exception {
        String mnemonic = "fortune gas smart clump wheel harbor mammal tragic ride together stand sugar debris lucky foil orphan giggle cement you raven purpose south food spatial";
        BigInteger bigInteger = new BigInteger("13525868411732188093555300818543209985500227142899933419203199820406980446096");
        byte[] msg = "Hello world!".getBytes(StandardCharsets.UTF_8);
        // signed by pure bouncy -- beg
        Ed25519PrivateKeyParameters privateKeyParameters = new Ed25519PrivateKeyParameters(bigInteger.toByteArray(), 0);
        Ed25519PublicKeyParameters publicKeyParameters = privateKeyParameters.generatePublicKey();
        Signer signer = new Ed25519Signer();
        signer.init(true, privateKeyParameters);
        signer.update(msg, 0, msg.length);
        byte[] signature = signer.generateSignature();
        System.out.println(Hex.encodeHex(signature));
        signer.init(false, publicKeyParameters);
        signer.update(msg, 0, msg.length);
        System.out.println(signer.verifySignature(signature));
        // signed by jca
        PrivateKey privateKey = Ed25519.getInstance().genPrivateKeyFromMnemonic(mnemonic);
        Signature edDSA = Signature.getInstance("EdDSA");
        edDSA.initSign(privateKey);
        edDSA.update(msg);
        byte[] sign = edDSA.sign();
        System.out.println(Hex.encodeHex(sign));
        PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed25519), new DEROctetString(privateKey.getEncoded()));
        JcaPEMKeyConverter pkcs8pemKeyConverter = new JcaPEMKeyConverter().setProvider("BC");
        JcaPKCS8EncryptedPrivateKeyInfoBuilder builder = new JcaPKCS8EncryptedPrivateKeyInfoBuilder(privateKey);
        Ed25519PrivateKeyParameters privateKeyParameters2 = new Ed25519PrivateKeyParameters(privateKeyInfo.parsePrivateKey().toASN1Primitive().getEncoded(), 0);
        Ed25519PublicKeyParameters publicKeyParametersGened = privateKeyParameters2.generatePublicKey();
        signer.init(false, publicKeyParametersGened);
        signer.update(msg, 0, msg.length);
        System.out.println(signer.verifySignature(signature));

    }

    @Test
    void ed25519Test() throws Exception{
        String mnemonic = "cruise subway unit feature property about scorpion grunt account prefer utility glide degree minimum elder simple consider program emerge page boat tourist wasp dentist";
        Algorithm algorithm = AlgorithmFactory.getAlgorithm(AlgorithmType.ED25519);
        PrivateKey key = algorithm.genPrivateKeyFromMnemonic(mnemonic);
        PublicKey publicKey = algorithm.genPublicKey(key);
        String s = algorithm.genAddressFromPublicKey("xchain", publicKey);
        System.out.println(s);
        byte[] sign = algorithm.sign(key, "Helloworld".getBytes());
        boolean b = algorithm.verifySignature(publicKey, "Helloworld".getBytes(), sign);
        System.out.println(b);
    }

    @Test
    void derTest() throws Exception{
        DEROctetString derOctetString = new DEROctetString("Hello".getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(derOctetString.getEncoded()));
        DEROctetString derOctetString2 = new DEROctetString(new BigInteger("11111").toByteArray());
        System.out.println(new BigInteger(derOctetString2.getOctets()));
    }

    @Test
    void addressSingleTest() throws Exception{
        String mnemonic = "robust rug bitter adjust lonely shed harbor gorilla picnic where fancy must tape old enter notice physical benefit soda immense mixed release column urban";
        SM2 algorithm = SM2.getInstance();
        PrivateKey privateKey = algorithm.genPrivateKeyFromMnemonic(mnemonic);
        PublicKey publicKey = algorithm.genPublicKey(privateKey);
        String address = algorithm.genAddressFromPublicKey("xchain", publicKey);
        System.out.println(address);
    }

    @Test
    void testIntegerBytes() throws DecoderException {
        BigInteger bigInteger = new BigInteger("13525868411732188093555300818543209985500227142899933419203199820406980446096");
        byte[] bytes = bigInteger.toByteArray();
        System.out.println();
        byte[] bytes1 = new BigInteger("258").toByteArray();
        byte[] bytes2 = new byte[]{0,1,-128};
        BigInteger test = new BigInteger(bytes2);
        System.out.println(test);
        BigInteger test2 = new BigInteger(new byte[]{1,127});
        String hex = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141";
        String half = "7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0";
        BigInteger bigInteger1 = new BigInteger(1, Hex.decodeHex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141".toCharArray()));
        BigInteger bigInteger2 = new BigInteger(1, Hex.decodeHex(half.toCharArray()));
        System.out.println(bigInteger1);
        System.out.println(bigInteger2);
    }
}
