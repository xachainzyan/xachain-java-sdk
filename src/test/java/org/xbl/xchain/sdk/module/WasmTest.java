package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.java.Log;
import org.junit.Test;
import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.block.BlockInfo;
import org.xbl.xchain.sdk.module.member.types.PermissionPolicy;
import org.xbl.xchain.sdk.module.wasm.querier.ContractHistory;
import org.xbl.xchain.sdk.module.wasm.querier.ContractInfo;
import org.xbl.xchain.sdk.module.wasm.types.ContractFileUtil;
import org.xbl.xchain.sdk.tx.TxInfo;
import org.xbl.xchain.sdk.types.TxResponse;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.xbl.xchain.sdk.module.Config.*;

@Log
public class WasmTest {
    public WasmTest() throws Exception {
    }

    @Test
    public void testInstantiateContract() throws Exception {
        String contractName = "test1";
        String language = "rust";
        File contractFile = new File("G:\\share\\patient.wasm");
        byte[] wasmBytes = ContractFileUtil.getContent(contractFile);
        String executePerm = "";
        String lable = "";
        TxResponse txResponse = xchainClient.instantiateContract(org1AdminAcc, contractName, language, wasmBytes, new JSONObject().toJSONString(), PermissionPolicy.POLICY_DROP, executePerm, lable);
        System.out.println(JSON.toJSONString(txResponse));
    }
    @Test
    public void testExecuteContract() throws Exception {
        String contractName = "test";
        String functionName = "create";
        String args = "{\"item\":{\"key\":\"2\",\"value\":\"2\"}}";
        TxResponse txResponse = xchainClient.executeContract(jackAcc, contractName, functionName, args);
        System.out.println(JSON.toJSONString(txResponse, true));
        assert txResponse.isSuccess();
    }
    @Test
    public void testmigrateContract() throws Exception {
        String contractName = "test4";
        String language = "rust";
        File contractFile = new File("G:\\share\\item.wasm");
        byte[] wasmBytes = ContractFileUtil.getContent(contractFile);
        String executePerm = "";
        String lable = "";
        TxResponse txResponse = xchainClient.migrateContract(org1AdminAcc, contractName, wasmBytes, new JSONObject().toJSONString(), PermissionPolicy.POLICY_DROP, executePerm);
        System.out.println(JSON.toJSONString(txResponse));
        assert txResponse.isSuccess();
    }
    @Test
    public void quertTx() throws Exception {
        TxInfo txInfo = xchainClient.queryTx("663FA1CBE6C86005EC2D92BFD9EBA0CB2967691816ECA5BD34EAD604AF0AAAF2");
        System.out.println(JSON.toJSONString(txInfo, true));
    }

    @Test
    public void testQueryTxhash() throws Exception {
        String url = "http://144.7.111.65:26657/";
        String chainId = "xachain";
        String mainPrefix = "xchain";
        SysConfig sysConfig = new SysConfig(url, chainId);
        XchainClient xchainClient = new XchainClient(sysConfig);
        String txhash = "F6885A72DB7C21EDEA9FF47182DA95D72B81BE576329EA8AFEF580FEAF1AC0A6";
        TxInfo txInfo = xchainClient.queryTx(txhash);
        System.out.println(123);
    }
    @Test
    public void testDestroyContract() throws Exception {
        String contractName = "testContract";
        TxResponse txResponse = xchainClient.destroyContract(jackAcc, contractName);
        assert txResponse.isSuccess();
    }

    @Test
    public void testUpdateContractPermission() throws Exception {
        String contractName = "testContract";
        String newAdmin = "sdgagagas";
        String executePerm = "";
        TxResponse txResponse = xchainClient.updateContractPermission(jackAcc, contractName, newAdmin, PermissionPolicy.POLICY_DROP, executePerm);
        assert txResponse.isSuccess();
    }

    @Test
    public void testQueryContract1() throws Exception {
        String contractName = "test4";
        String args = "{\"find\":{\"key\":\"5\"}}";
        String result = xchainClient.queryContract(contractName, JSONObject.parseObject(args, Map.class));
        System.out.println(result);
    }
    @Test
    public void testQueryContract() throws Exception {
        String contractName = "item";
        String functionName = "find";
        String args = "{\"key\":\"5\"}";
        String result = xchainClient.queryContract(contractName, functionName, args);
        System.out.println(result);
    }


    @Test
    public void testQueryContractInfos() throws Exception {
        List<ContractInfo> contractInfos = xchainClient.queryContractInfos();
        System.out.println(123);
    }

    @Test
    public void testQueryContractInfo() throws Exception {
        String contractName = "testContract";
        ContractInfo contractInfo = xchainClient.queryContractInfo(contractName);
    }

    @Test
    public void testQueryContractHistory() throws Exception {
        String contractName = "testContract";
        List<ContractHistory> contractHistories = xchainClient.queryContractHistory(contractName);
    }

}
