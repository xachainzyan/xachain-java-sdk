package org.xbl.xchain.sdk.module;

import lombok.extern.java.Log;
import org.junit.Test;
import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.module.auth.querier.AuthAccount;
import org.xbl.xchain.sdk.types.Account;
import org.xbl.xchain.sdk.types.KeyInfo;
import org.xbl.xchain.sdk.types.TxResponse;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.xbl.xchain.sdk.module.Config.*;
@Log
public class BankTest {

    @Test
    public void testSend() throws Exception {
        TxResponse txResponse = xchainClient.transfer(jackAcc, org1AdminAcc.getAddress(), "stake", "1");
        log.info(txResponse.toString());
        assert txResponse.isSuccess();
    }

    @Test
    public void testBench() throws Exception {
        long l = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(16);
        for (int i = 0; i < 1000000; i++) {
            executorService.submit(()->{
                try {
                    xchainClient.transfer(jackAcc, org1AdminAcc.getAddress(), "stake", "100");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        Thread.sleep(100000);
    }

    @Test
    public void testQueryAuthAccount() throws Exception {
        AuthAccount authAccount = xchainClient.queryAuthAccount(jackAcc.getKeyInfo().getAddress());
        log.info(authAccount.toString());
        authAccount = xchainClient.queryAuthAccount(org1AdminAcc.getKeyInfo().getAddress());
        log.info(authAccount.toString());
    }
}
