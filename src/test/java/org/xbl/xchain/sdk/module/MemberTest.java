package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import lombok.extern.java.Log;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.xbl.xchain.sdk.amino.Codec;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.exception.WrongQueryParamException;
import org.xbl.xchain.sdk.module.member.msg.MsgAddAccount;
import org.xbl.xchain.sdk.module.member.msg.MsgEditAccountRole;
import org.xbl.xchain.sdk.module.member.querier.*;
import org.xbl.xchain.sdk.module.member.types.ExpElement;
import org.xbl.xchain.sdk.module.member.types.PermissionPolicy;
import org.xbl.xchain.sdk.tx.StdTx;
import org.xbl.xchain.sdk.types.KeyInfo;
import org.xbl.xchain.sdk.types.TxResponse;

import java.util.Arrays;
import java.util.List;

import static org.xbl.xchain.sdk.module.Config.*;

@Log
public class MemberTest {

    @Test
    public void testAddAccount() throws Exception {
        KeyInfo keyInfo = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix);
        System.out.println(keyInfo.getAddress());
        TxResponse txResponse = xchainClient.addAccount(org1AdminAcc, keyInfo.getAddress(), "ORG49f9143a41f14e8583b6b0efaf4193fb", "client");
        System.out.println(txResponse.toString());
    }

    @Test
    public void testRevokeAccount() throws Exception {
        String address = "xchain1vksw5yupvje76unc22vnyrd6pqq7dr3c8c5u7a";
        TxResponse txResponse1 = xchainClient.revokeAccount(org1AdminAcc, address);
        assert txResponse1.isSuccess();
        log.info(String.format("revokeAccount %s", address));
    }

    @Test
    public void testAddMultipleAccounts() throws Exception {
        MsgAddAccount msgAddAccount = new MsgAddAccount(xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress(), "org1", "client", org1AdminAcc.getAddress());
        MsgAddAccount msgAddAccount1 = new MsgAddAccount(xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress(), "org1", "client", org1AdminAcc.getAddress());
        MsgAddAccount msgAddAccount2 = new MsgAddAccount(xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress(), "org1", "client", org1AdminAcc.getAddress());
        List<MsgAddAccount> msgAddAccounts = Arrays.asList(msgAddAccount, msgAddAccount1, msgAddAccount2);
        TxResponse txResponse = xchainClient.addMultipleAccounts(org1AdminAcc, msgAddAccounts);
        assert txResponse.isSuccess();
        log.info(String.format("addMultipleAccounts %s %s %s", msgAddAccount.getAddress(), msgAddAccount1.getAddress(), msgAddAccount2.getAddress()));
    }

    @Test
    public void testEditAccountRole() throws Exception {
        TxResponse txResponse = xchainClient.editAccountRole(org1AdminAcc, "xchain1yxmf0kyx9n8gt5zumfewemlwk9r829hrdx4rgv", "client", MsgEditAccountRole.EditType.REVOKEROLE);
        assert txResponse.isSuccess();
        Thread.sleep(5000);
        TxResponse txResponse1 = xchainClient.editAccountRole(org1AdminAcc, "xchain1yxmf0kyx9n8gt5zumfewemlwk9r829hrdx4rgv", "client", MsgEditAccountRole.EditType.ADDROLE);
        assert txResponse1.isSuccess();
    }

    @Test
    public void testFreezeAccount() throws Exception {
        TxResponse txResponse = xchainClient.freezeAccount(org1AdminAcc, "xchain1yxmf0kyx9n8gt5zumfewemlwk9r829hrdx4rgv", null, null);
        assert txResponse.isSuccess();
    }

    @Test
    public void testUnfreezeAccount() throws Exception {
        TxResponse txResponse = xchainClient.unfreezeAccount(org1AdminAcc, "xchain1yxmf0kyx9n8gt5zumfewemlwk9r829hrdx4rgv", null, null);
        assert txResponse.isSuccess();
    }

    @Test
    public void testAddGateway() throws Exception {
        String address = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress();
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.addGateway(jackAcc, address, after60s, after60s);
        assert txResponse.isSuccess();
        log.info(String.format("addGateway %s", address));
    }

    @Test
    public void testRevokeGateway() throws Exception {
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.revokeGateway(jackAcc, "xchain1yxmf0kyx9n8gt5zumfewemlwk9r829hrdx4rgv", after60s, after60s);
        assert txResponse.isSuccess();
    }

    @Test
    public void testAddNetworkAdmin() throws Exception {
        String address = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress();
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        Integer power = 1;
        TxResponse txResponse = xchainClient.addNetworkAdmin(jackAcc, address, power, after60s, after60s);
        assert txResponse.isSuccess();
        log.info(String.format("addNetworkAdmin %s", address));
    }

    @Test
    public void testRevokeNetworkAdmin() throws Exception {
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.revokeNetworkAdmin(jackAcc, "xchain1vtwnezlgnelgrrmtmpdpshcmaev8p8zzuhjqh6", after60s, after60s);
        assert txResponse.isSuccess();
    }

    @Test
    public void testAddOrg() throws Exception {
        String orgAdminAddress = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress();
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.addOrg(jackAcc, "newOrg1", orgAdminAddress, true, 1, after60s, after60s);
        assert txResponse.isSuccess();
        log.info(String.format("addOrg %s", orgAdminAddress));
    }

    @Test
    public void testRevokeOrg() throws Exception {
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.revokeOrg(jackAcc, "newOrg1", after60s, after60s);
        assert txResponse.isSuccess();
    }

    @Test
    public void testAddSubOrg() throws Exception {
        String orgAdminAddress = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress();
        TxResponse txResponse = xchainClient.addSubOrg(org1AdminAcc, "subOrg1", "org1", orgAdminAddress);
        assert txResponse.isSuccess();
        log.info(String.format("addSubOrg %s", orgAdminAddress));
    }

    @Test
    public void testRevokeSubOrg() throws Exception {
        TxResponse txResponse = xchainClient.revokeSubOrg(org1AdminAcc, "org1.subOrg1");
        assert txResponse.isSuccess();
    }

    @Test
    public void testFreezeOrg() throws Exception {
        TxResponse txResponse = xchainClient.freezeOrg(org1AdminAcc, "org1.subOrg1");
        assert txResponse.isSuccess();
    }
    @Test
    public void testQueryAccount() throws Exception {
        AccountInfo accountInfo = xchainClient.queryMemberAccounts("xchain1h2aemqx7f8nl6vhecwty8atmhajgz38r2yj0j7");
        System.out.println(accountInfo);
    }

    @Test
    public void testUnFreezeOrg() throws Exception {
        TxResponse txResponse = xchainClient.unfreezeOrg(org1AdminAcc, "org1.subOrg1");
        assert txResponse.isSuccess();
    }

    @Test
    public void testChangeOrgAdmin() throws Exception {
        String orgAdminAddress = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix).getAddress();
        TxResponse txResponse = xchainClient.changeOrgAdmin(org1AdminAcc, "org1.subOrg1", orgAdminAddress);
        assert txResponse.isSuccess();
        log.info(String.format("changeOrgAdmin %s", orgAdminAddress));
    }

    @Test
    public void testAddRole() throws Exception {
        TxResponse txResponse = xchainClient.addRole(org1AdminAcc, "caiwu", "client", "org1");
        assert txResponse.isSuccess();
    }

    @Test
    public void testRemoveRole() throws Exception {
        TxResponse txResponse = xchainClient.removeRole(org1AdminAcc, "org1", "caiwu");
        assert txResponse.isSuccess();
    }

    @Test
    public void testSavePermission() throws Exception {
        String resource = "member_test";
        Long after60s = System.currentTimeMillis() / 1000 + 60;

        ExpElement expElement = new ExpElement();
//        expElement.setOrgExps(new String[]{"orgs"});
        expElement.setAddresses(new String[]{"addresses"});
//        expElement.setRoleIds(new String[]{"roleids"});
        expElement.setChainIds(new String[]{"chainids"});
        ExpElement[] expElements = new ExpElement[]{expElement};


        TxResponse txResponse = xchainClient.savePermission(jackAcc, resource, PermissionPolicy.POLICY_DROP, expElements, after60s, after60s);
        assert txResponse.isSuccess();
    }
    @Test
    public void testSavePermission1() throws Exception {
        String tx = "c601cea622740a4c5cea1fae0a0b6d656d6265725f7465737410021a150a08636861696e696473220961646472657373657322141a93e8cef10ae8b19e68dedd33d75069b73a6ab4289dd286a306309dd286a3061206108094ebdc031a6a0a26e7cd5a35210257fd53d353f9c61560ef81842c401885f92861b794cbfac078f884ea83b48b2112409f07ee15d30b66c673f34dbbc94a7e47d981892d6c5e7f32b02a3ec88d2bebc8a95f7fe2b1976f4a2f6da356ea02139a825d962d8c3098322eb82a530cec9d66";
        byte[] txBytes = Hex.decode(tx);
        StdTx stdTx = Codec.getAmino("2").unmarshalBinaryLengthPrefixed(txBytes, StdTx.class);
        System.out.println(1);
    }

    @Test
    public void testQueryMemberAccounts() throws WrongQueryParamException {
        List<AccountInfo> accountInfos = xchainClient.queryMemberAccounts();
        assert accountInfos.size() > 0;
        AccountInfo accountInfo = xchainClient.queryMemberAccounts(accountInfos.get(0).getAddress());
        assert accountInfo != null;
    }

    @Test
    public void testQueryOrgs() throws WrongQueryParamException {
        List<OrgInfo> orgInfos = xchainClient.queryOrgs();
        assert orgInfos.size() > 0;
        OrgInfo orgInfo = xchainClient.queryOrgs(orgInfos.get(0).getOrgFullId());
        assert orgInfo != null;
    }

    @Test
    public void testQueryPermissions() throws WrongQueryParamException {
        List<PermissionInfo> permissionInfos = xchainClient.queryPermissions();
        assert permissionInfos.size() > 0;
        PermissionInfo permissionInfo = xchainClient.queryPermissions(permissionInfos.get(0).getResource());
        assert permissionInfo != null;
    }

    @Test
    public void testQueryRoles() throws WrongQueryParamException {
        List<RoleInfo> roleInfos = xchainClient.queryRoles();
        assert roleInfos.size() > 0;
        RoleInfo roleInfo = xchainClient.queryRoles(roleInfos.get(0).getRoleFullId());
        assert roleInfo != null;
    }

    @Test
    public void testCheckPermission() throws WrongQueryParamException {
        CheckPermissionResult checkPermissionResult = xchainClient.checkPermission("member_addAccount", org1AdminAcc.getAddress());
        assert checkPermissionResult.getOk();
        checkPermissionResult = xchainClient.checkPermission("member_addAccount", jackAcc.getAddress());
        assert !checkPermissionResult.getOk();
    }
}
