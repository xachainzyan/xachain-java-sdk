package org.xbl.xchain.sdk.module;

import lombok.extern.java.Log;
import org.junit.Test;
import org.xbl.xchain.sdk.module.gov.querier.ProposalInfo;
import org.xbl.xchain.sdk.module.gov.types.VoteType;
import org.xbl.xchain.sdk.types.TxResponse;

import java.util.List;

import static org.xbl.xchain.sdk.module.Config.jackAcc;
import static org.xbl.xchain.sdk.module.Config.xchainClient;

@Log
public class GovTest {

    public GovTest() throws Exception {
    }

    @Test
    public void testVote() throws Exception {
        TxResponse txResponse = xchainClient.voteProposal(jackAcc, "proposalId", VoteType.PASS);
        log.info(txResponse.toString());
        assert txResponse.isSuccess();
    }
    @Test
    public void testVoidProposal() throws Exception {
        TxResponse txResponse = xchainClient.voidProposal(jackAcc, "proposalId");
        assert txResponse.isSuccess();
    }
    @Test
    public void testQuertProposals() throws Exception {
        List<ProposalInfo> proposalInfos = xchainClient.queryProposals();
        assert proposalInfos.size() > 0;
        ProposalInfo proposalInfo = xchainClient.queryProposals(proposalInfos.get(0).getProposalId());
        assert proposalInfo != null;
    }
}
