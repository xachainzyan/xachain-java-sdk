package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.xbl.xchain.sdk.block.BlockInfo;
import org.xbl.xchain.sdk.block.BlockMetas;
import org.xbl.xchain.sdk.module.wasm.querier.ContractInfo;
import org.xbl.xchain.sdk.tx.TxInfo;

import java.util.List;

import static org.xbl.xchain.sdk.module.Config.xchainClient;

public class BlockTest {

    @Test
    public void testGetBlock() throws Exception {
        TxInfo txInfo = xchainClient.queryTx("F3EB96798120CF46A755CEBEBD6F7F0059700E0612647D604D512B72B6C4C4B8");
//        BlockInfo blockInfo1 = xchainClient.queryBlockByHash("64DFB648D1CC8712CD8FF3ACFAA960A6BB17AA76C0488FEC45992A22156152AA");
//        BlockMetas blockMetas = xchainClient.queryBlock("1", "5");
//        BlockInfo latestBlock = xchainClient.getLatestBlock();
//        System.out.println(latestBlock);
        BlockInfo blockInfo = xchainClient.queryBlock("11076");
        System.out.println(blockInfo.getTxs().get(0).getTxHash());
        System.out.println(blockInfo.getBlockHash());
        System.out.println(blockInfo.getBlockHeight());
        System.out.println(blockInfo.getBlockTime());
        System.out.println(JSON.toJSONString(blockInfo.getTxs().get(0).getMsg()));
        System.out.println(JSON.toJSONString(blockInfo.getTxs()));
    }
    @Test
    public void testGetValidators() throws Exception {
        String s = xchainClient.queryTendermintValidators();
        System.out.println(s);
    }
}
