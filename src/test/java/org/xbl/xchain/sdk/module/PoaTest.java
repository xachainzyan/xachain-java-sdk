package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.java.Log;
import org.junit.Test;
import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.amino.Codec;
import org.xbl.xchain.sdk.block.BlockInfo;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.exception.WrongQueryParamException;
import org.xbl.xchain.sdk.module.consensus.querier.ValidatorInfo;
import org.xbl.xchain.sdk.module.consensus.types.Description;
import org.xbl.xchain.sdk.tx.StdTx;
import org.xbl.xchain.sdk.tx.TxInfo;
import org.xbl.xchain.sdk.types.KeyInfo;
import org.xbl.xchain.sdk.types.TxResponse;

import java.io.Serializable;
import java.util.Base64;
import java.util.List;

import static org.xbl.xchain.sdk.module.Config.*;

@Log
public class PoaTest {
    public PoaTest() throws Exception {
    }

    @Test
    public void testCreateValidator() throws Exception {
        Description description = new Description();
        description.setDetails("oh");
        KeyInfo keyInfo = xchainClient.generateKeyInfo(AlgorithmType.ED25519, mainPrefix);
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.createValidator(jackAcc, description, keyInfo.getConsensusPubKey(), 1, after60s, after60s);
        System.out.println(JSON.toJSONString(txResponse));
        System.out.println(keyInfo.getConsensusPubKey());
        System.out.println(keyInfo.getValidatorAddress());
        System.out.println(keyInfo.toNodeKeyFile());
        System.out.println(keyInfo.toPrivValidatorKeyFile());
        assert txResponse.isSuccess();
    }

    @Test
    public void testEditValidator() throws Exception {
        String validatorAddress = "xchainvaloper1f2dsm5xrzqy8duuuslf6tjxl0wmur9z0ullwwj";
        Long after60s = System.currentTimeMillis() / 1000 + 60;
        TxResponse txResponse = xchainClient.editValidator(jackAcc, validatorAddress, 1, after60s, after60s);
        assert txResponse.isSuccess();
    }

    @Test
    public void testQueryValidators() throws WrongQueryParamException {
        List<ValidatorInfo> validatorInfos = xchainClient.queryValidators();
        assert validatorInfos.size() > 0;
        ValidatorInfo validatorInfo = xchainClient.queryValidators(validatorInfos.get(0).getValidatorAddress());
        assert validatorInfo != null;
    }

    @Test
    public void testQueryTx() throws WrongQueryParamException {
        TxInfo txInfo = xchainClient.queryTx("D66069DC28326BF54EDADBA293491C153A8296B2D9E61BAB75E982230E545CAA");
        System.out.println(JSON.toJSONString(txInfo, true));
        assert txInfo != null;
    }

    @Test
    public void testQueryTxspage() throws WrongQueryParamException {
        List<TxInfo> txInfos = xchainClient.queryTxs(1, 10);
    }

    @Test
    public void testQueryTxs() throws WrongQueryParamException {
        List<TxInfo> txInfo = xchainClient.queryTxs(93L);
        System.out.println(JSON.toJSONString(txInfo, true));
        assert txInfo != null;
    }

    @Test
    public void testQueryBlock() throws WrongQueryParamException {
        BlockInfo blockInfo = xchainClient.queryBlock(null);
        List<String> txs = blockInfo.getBlock().getData().getTxs();
        byte[] txData = Base64.getDecoder().decode(txs.get(0));
        StdTx stdTx = Codec.getAmino().unmarshalBinaryLengthPrefixed(txData, StdTx.class);
        System.out.println(1);
    }
}
