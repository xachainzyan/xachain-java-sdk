package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.java.Log;
import org.junit.Test;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.types.TxResponse;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.xbl.xchain.sdk.module.Config.*;

@Log
public class CelerainTest {

    @Test
    public void testPutCelerain() throws Exception {
        TxResponse txResponse = xchainClient.putCelerain(org1AdminAcc, "a", "abc");
        assert txResponse.isSuccess();
        log.info(txResponse.getHash());
    }

    @Test
    public void testQueryCelerain1() throws Exception {
        String result = xchainClient.queryCelerain("a");
        System.out.println(result);

    }

    @Test
    public void testQueryCelerain() throws Exception {
        int cpunum = Runtime.getRuntime().availableProcessors();
        for (int i = 1; i <= cpunum; i++) {
            Integer seq = i;
            for (int nn = 1; nn <= 100; nn++) {
                String key = seq + "_" + nn;
                String result = xchainClient.queryCelerain(key);
                System.out.println("key: " + key + " result: " + result);
                assert key.equals(result);
            }
        }

    }

    @Test
    public void testStress() throws Exception {
        Random random = new Random();
        int cpunum = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(cpunum, cpunum, 1, TimeUnit.HOURS, new ArrayBlockingQueue<Runnable>(1000000));
        CountDownLatch countDownLatch = new CountDownLatch(cpunum);
        for (int i = 1; i <= cpunum; i++) {
            Integer seq = i;
            threadPoolExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    for (int nn = 1; nn <= 10000; nn++) {
                        String key = seq + "_" + nn;
                        try {
                            xchainClient.putCelerain(org1AdminAcc, key, key);
//                            String args = "{\"key\":\""+key+"\",\"value\":"+value+"}";
//                            JSONObject params = new JSONObject();
//                            params.put("funcA", JSONObject.parseObject(args));
//                            xchainClient.executeContract(org1AdminAcc, "huohuoA",params.toJSONString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
    }
}
