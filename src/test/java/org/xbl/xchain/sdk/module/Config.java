package org.xbl.xchain.sdk.module;

import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.types.Account;
import org.xbl.xchain.sdk.types.KeyInfo;

public interface Config {
    String node = "http://144.7.99.192:15001";

    String chainId = "XCHAIN31d481ea4aa546898159a14c7a80384a";

    String mainPrefix = "xchain";

    String org1Admin = "硅 优 田 幸 塑 拿 九 重 全 农 流 起 仓 主 条 摩 柳 施 轰 妇 逐 眼 管 部";

    String jackMem = "ripple oxygen access media media rotate lazy ladder able aware shoot image strong shiver cruel flower exhibit cargo cement inform push mystery enter box";
    SysConfig sysConfig = new SysConfig(node, chainId, "1");
    XchainClient xchainClient = new XchainClient(sysConfig);
    Account jackAcc = Account.buildAccount(new KeyInfo(jackMem, AlgorithmType.SM2, mainPrefix));
    Account org1AdminAcc = Account.buildAccount(new KeyInfo(org1Admin, AlgorithmType.SM2, mainPrefix));
}
