package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.xbl.xchain.sdk.types.TxResponse;

import java.util.Date;

import static org.xbl.xchain.sdk.module.Config.*;

public class CrossTest {

    @Test
    public void testCrossContract() throws Exception {
//        TxInfo txInfo = xchainClient.queryTx("202DB2AE08FE75C47840CAF539BC5132D2E33E797DEC31B68E9CB36D95D34D40");
//        System.out.println(1);
        TxResponse txResponse = xchainClient.executeCrossContract(org1AdminAcc, "item", "create", "{}", "xchain1", "", "", "", "");
        System.out.println(JSON.toJSONString(txResponse));
    }

}
