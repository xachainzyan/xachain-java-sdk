package org.xbl.xchain.sdk.module;

import com.alibaba.fastjson.JSON;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.xbl.xchain.sdk.SysConfig;
import org.xbl.xchain.sdk.XchainClient;
import org.xbl.xchain.sdk.amino.Codec;
import org.xbl.xchain.sdk.block.BlockInfo;
import org.xbl.xchain.sdk.crypto.algo.AlgorithmType;
import org.xbl.xchain.sdk.module.gov.types.VoteType;
import org.xbl.xchain.sdk.module.iccp.types.AppChainInfo;
import org.xbl.xchain.sdk.tx.StdTx;
import org.xbl.xchain.sdk.types.Account;
import org.xbl.xchain.sdk.types.KeyInfo;
import org.xbl.xchain.sdk.types.TxResponse;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.List;

import static org.xbl.xchain.sdk.module.Config.*;

public class IccpTest {

    @Test
    public void testRegisterAppInfo() throws Exception {
        KeyInfo keyInfo = xchainClient.generateKeyInfo(AlgorithmType.SM2, mainPrefix);

        Long after60s = System.currentTimeMillis() / 1000 + 60;
        String chainId = "xachain1";
        String name = "xachain1";
        String chainType = "xchain";
        Integer consensusType = 1;
        String validators = "{}";
        String version = "0.11";
        String desc = "nodesc";
        String pubkey =keyInfo.getBech32PubKey();


        AppChainInfo appChainInfo = new AppChainInfo(chainId, name, chainType, consensusType, validators, version, desc, pubkey, 0, "");
        TxResponse txResponse = xchainClient.appChainRegister(jackAcc, appChainInfo, after60s, after60s);
        System.out.println(JSON.toJSONString(txResponse));
    }
    @Test
    public void testQueryAppChainInfo() throws Exception {
        String chainId = "XCHAIN3d757324287d4d938bbd27312177118b";
        AppChainInfo appChainInfo = xchainClient.queryAppChainInfo(chainId);
        System.out.println(JSON.toJSONString(appChainInfo));
    }
    @Test
    public void testBlock() throws Exception {
//        String tx = "yALOpiJ0CrMB4MwfSgoFYWRtaW4SFPLhGTsgJ+dVaSASYmh70NZHNUbQGoEBCgh4YWNoYWluMRIIeGFjaGFpbjEaBnhjaGFpbiAAKgJ7fTIEMC4xMToGbm9kZXNjQk14Y2hhaW5wdWIxdWx4NDVkZnBxMGF2eXE1cXEzdTg2cjM4MDlud3FzNnprcmVrajZ2d2M1c2VscGs0N25ycmhjOWNhd255dThmYzQ3a0gAIAAo7+OrnwYw7+OrnwYSBhCAlOvcAxpqCibnzVo1IQIazZBNlpwAAdlnZTHw9c71TP0W3HwHz85C6/JePv0UCBJAkD9CbJbJrJhOXJ5N6WByoT+6ZQz1LmdcB/5LG3Dglld8H2hqCyMnxkp4iriGxEY7TWXK0SglACs1KyCpyc99pCoY3VE8JTozoKhFv1ajtM5g0UR0DHLGEUnU";
        String tx = "yALOpiJ0CrMB4MwfSgoFYWRtaW4SFPLhGTsgJ+dVaSASYmh70NZHNUbQGoEBCgh4YWNoYWluMRIIeGFjaGFpbjEaBnhjaGFpbiABKgJ7fTIEMC4xMToGbm9kZXNjQk14Y2hhaW5wdWIxdWx4NDVkZnBxMDBkeWFhYXk5Y2p6OWxhbTlqYzNxZ3QzODZxZWNyM2cyc2c0c2dqNXJxbWYzOHVjdzB6cWRjM2gzekgAIAAotuirnwYwtuirnwYSBhCAlOvcAxpqCibnzVo1IQIazZBNlpwAAdlnZTHw9c71TP0W3HwHz85C6/JePv0UCBJAD5881DD7PfyZXOV/pDfheI9WpnNQYPAJIGIKDw6ndSjM9CgHfHM9Pe7BTtgyn8kzHRobfU0QU8zPTNqoFc8o7SoYzTW+5PXQVcubj3aQEZlaFf3jXpu6f9PK";
        byte[] txBytes = Base64.getDecoder().decode(tx.getBytes());
        String txHash = Hex.toHexString(MessageDigest.getInstance("SHA-256").digest(txBytes)).toUpperCase();
        System.out.println(txHash);
        StdTx stdTx = Codec.getAmino().unmarshalBinaryLengthPrefixed(txBytes, StdTx.class);

        System.out.println(1);
        BlockInfo blockInfo = xchainClient.queryBlock("3");
    }

    @Test
    public void testRegister() throws Exception {
        String node = "http://192.168.40.128:26610/";
        String jackMem = "obscure firm wink light talent wedding neither impulse life pen husband corn mansion south trumpet oppose food this toe thank that old hero merge";
        Account jackAcc = Account.buildAccount(new KeyInfo(jackMem, AlgorithmType.SM2, mainPrefix));
        String chainId = "xchain-hub";
        SysConfig sysConfig = new SysConfig(node, chainId);
        XchainClient xchainClient = new XchainClient(sysConfig);
//        AppChainInfo appChainInfo = new AppChainInfo();
//        appChainInfo.setChainId("xachain");
//        appChainInfo.setName("xachain");
//        appChainInfo.setChainType("xchain");
//        appChainInfo.setConsensusType(0);
//        appChainInfo.setValidators("[{\"address\":\"A2EA1372BE20CFFBC41C45EF4F50D42D17FAAB4B\",\"pub_key\":{\"type\":\"tendermint/PubKeySm2\",\"value\":\"AtAhPgzLADAVln7XcEKURHvgkNJJN1dBj2w8Ckeo0Mt6\"},\"voting_power\":\"100\",\"proposer_priority\":\"0\"}]");
//        appChainInfo.setVersion("1.3.0");
//        appChainInfo.setDesc("xachain");
//        appChainInfo.setPublicKey("xchainpub1ulx45dfpqfd2xt8mv9suzf3z7kkcs09uzm2cdh2ngs74lfx5rcu97marhy9n5mcva8w");
//        appChainInfo.setState(0);
//        appChainInfo.setProposalId("");
//        TxResponse txResponse = xchainClient.appChainRegister(jackAcc, appChainInfo, System.currentTimeMillis() / 1000 + 180, System.currentTimeMillis() / 1000 + 180);
//        System.out.println(JSON.toJSONString(txResponse));

//        TxResponse txResponse1 = xchainClient.voteProposal(jackAcc, "iccp_xchain1k2pd5998xhahgtwyxsjmyn362qn5r9ve80m3d4_1676443163", VoteType.PASS);
//        System.out.println(JSON.toJSONString(txResponse1));

        AppChainInfo appChainInfo = xchainClient.queryAppChainInfo("xachain");
        System.out.println(JSON.toJSONString(appChainInfo));
    }
    @Test
    public void testAddGateway() throws Exception {
        String node = "http://192.168.40.128:26657/";
        String jackMem = "tiger scatter margin balance riot speed album then hamster slight suspect eagle bargain reform inject pave member evidence strategy fiction display city level speed";
        Account jackAcc = Account.buildAccount(new KeyInfo(jackMem, AlgorithmType.SM2, mainPrefix));
        String chainId = "xachain";
        SysConfig sysConfig = new SysConfig(node, chainId);
        XchainClient xchainClient = new XchainClient(sysConfig);
        TxResponse txResponse = xchainClient.addGateway(jackAcc, "xchain1scxe02fz8s95226f0y4ukt38kr0kh5m6zyx98c", System.currentTimeMillis() / 1000 + 60, System.currentTimeMillis() / 1000 + 60);
        System.out.println(JSON.toJSONString(txResponse));
        TxResponse txResponse1 = xchainClient.voteProposal(jackAcc, txResponse.getProposalId(), VoteType.PASS);
        System.out.println(JSON.toJSONString(txResponse1));
    }
}
